#!/usr/bin/env python
# coding: utf-8

# In[1]:


print('Start')
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import datetime

import pandas as pd
import numpy as np
print('imports over')

# In[2]:


cred = credentials.Certificate('ServiceAccountKey.json')
firebase_admin.initialize_app(cred,{'databaseURL': 'https://recuperapp-8596a.firebaseio.com/'})


# In[3]:


db = firestore.client()
def getDocuments(path,withId=False):
    users_ref = db.collection(path)
    docs = users_ref.stream()

    data = []
    cols = set()
    for doc in docs:
        a = doc.to_dict()
        if(withId):
            a['id']=doc.id
        cols.update(a.keys())
        data.append(a)
    return pd.DataFrame(data,columns=cols)


# In[4]:

print('usuarios')
usuarios = getDocuments(u'usuarios')
usuarios = usuarios.drop(columns=['img','imagenes','id','chat'])


# In[5]:

print('ejercicios')
ejercicios = getDocuments(u'ejercicios',withId=True)
dummies = pd.get_dummies(ejercicios['musculos'].apply(pd.Series).stack()).sum(level=0)
ejercicios = ejercicios.drop(columns=['img','descripcion','musculos'])
ejercicios = pd.concat([ejercicios,dummies], axis=1, sort=False)


# In[6]:

print('doctores')
doctores = getDocuments(u'doctores',withId=True)
doctores['numeroPacientes'] = doctores['pacientes'].apply(lambda x:len(x))


# In[7]:

print('chats')
a = [] 
for index, row in doctores.iterrows():
    id = row['id']
    for id2 in row['pacientes']:
        idChat = '{}-{}'.format(id,id2)
        a.append({
            'doctor':id,
            'user':id2,
            'chat':idChat
        })
chats = pd.DataFrame(a)
doctores = doctores.drop(columns=['pacientes'])


# In[8]:


response = []
for h in chats['chat']:
    a = getDocuments(u'chats/{}/msgs'.format(h))
    a.sort_values(by='date',inplace =True)
    a = a.reset_index(drop=True)
    ind = pd.Series([i for i in range(len(a))])[(a['user']==0) & (a.index>0)].values
    b = (a['date'][ind].values-a['date'][ind-1].values).mean().astype('timedelta64[s]').item().seconds
    response.append(b)
chats['velocidadRespuesta']=response


# In[9]:


vel = chats.groupby('doctor').mean().reset_index()
vel.columns = ['id','velocidadDeRespuesta']
doctores = pd.merge(doctores, vel, on='id')


# In[10]:

print('task(this takes a lot of time)')

data = []
for doc in db.collection('tareas').list_documents():
    userid = doc.id
    print(userid)
    for col in doc.collections():
        date = datetime.datetime.strptime(col.id,'%d-%m-%Y')
        for doc in col.list_documents():
            a = doc.get().to_dict()
            a['userId'] = userid
            a['date'] = date
            data.append(a)


# In[11]:


task = pd.DataFrame(data)
task['tiempoDone'] = (task['tiempo']*task['done']).astype(float)
task['dayOfWeek'] = task['date'].apply(lambda x:x.weekday())


# In[112]:
print('calculated tables')

UserTaskCum = task[['userId','date','tiempo','tiempoDone']].groupby(['userId','date']).sum().reset_index()
A = UserTaskCum[['userId','date']].groupby('userId').min().reset_index()
A.columns= ['userId','minDate']
UserTaskCum = pd.merge(A,UserTaskCum,on='userId')
UserTaskCum['minDate'] = UserTaskCum['date'] -UserTaskCum['minDate']
UserTaskCum['minDate'] = UserTaskCum['minDate'].apply(lambda x:x.days)


# In[113]:


DayOfWeekCount = task[['dayOfWeek','id','userId','date']].groupby(['date','userId','dayOfWeek']).count().groupby('dayOfWeek').mean().reset_index()
DayOfWeekCount.columns= ['dayOfWeek','count']


# In[114]:


ExerciseAssigment = pd.merge(task[['date','id']],ejercicios[['id','nombre']],on='id')
ExerciseAssigment = ExerciseAssigment.groupby(['date','nombre']).count().reset_index()
ExerciseAssigment.columns = ['date','ejercicio','count']


# In[115]:


from firebase_admin import db
ref = db.reference('/')
def save(name,X):
    print(name)
    print(X.head())
    
    import io
    s = io.StringIO()
    X.to_csv(s,index =False)
    ref.child(name).set(s.getvalue().replace('\r\n','|')[:-1])


# In[116]:


save('usuarios',usuarios)
save('ejercicios',ejercicios)
save('doctores',doctores)
save('chats',chats)
save('UserTaskCum',UserTaskCum)
save('DayOfWeekCount',DayOfWeekCount)
save('ExerciseAssigment',ExerciseAssigment)
save('task',task[['id', 'tiempo', 'tipo', 'nombre', 'userId', 'date','tiempoDone', 'dayOfWeek']])


# In[ ]:




