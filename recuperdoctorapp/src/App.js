import React from 'react';
import './App.css';
import { Navbar, Nav, Row, Col, Container } from "react-bootstrap";
import LogginForm from './components/LogginForm'

function App() {
  return (
    <div>
      <Navbar bg="primary" expand="lg" variant="dark">
        <Navbar.Brand>RecuperApp</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto" />
          <LogginForm></LogginForm>
        </Navbar.Collapse>
      </Navbar>
      <Container>
        <Row>
          <Col>1 of 2</Col>
          <Col>2 of 2</Col>
        </Row>
        <Row>
          <Col>1 of 3</Col>
          <Col>2 of 3</Col>
          <Col>3 of 3</Col>
        </Row>
      </Container>
     
      <p>hello</p>
    </div>

  );
}

export default App;
