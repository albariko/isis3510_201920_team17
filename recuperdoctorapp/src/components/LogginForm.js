import React from 'react';
import { Form, FormControl, Button } from "react-bootstrap";

class LogginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { correo: '', cont: '' ,correoError:'error'};

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    alert('A name was submitted: ' + this.state.correo + " " + this.state.cont);
    console.log('asd');
    event.preventDefault();
  }

  render() {
    return (
      <Form inline onSubmit={this.handleSubmit}>
        <FormControl type="text" placeholder="Correo" className=" mr-sm-2" value={this.state.correo} onChange={(event) => this.setState({ correo: event.target.value })} />
        {this.state.correoError!=='' && (<p>{this.state.correoError}</p>)}
        <FormControl type="text" placeholder="Contraseña" className=" mr-sm-2" value={this.state.cont} onChange={(event) => this.setState({ cont: event.target.value })} />
        <Button type="ingresar" className="btn btn-success">Ingresar</Button>
      </Form>
    );
  }
}

export default LogginForm;