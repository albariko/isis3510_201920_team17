class Exercise {
  const Exercise(
      {this.id,
      this.name,
      this.duracion,
      this.descripcion,
      this.imgSource,
      this.repetitions,
      this.muscles});
  final String id;
  final String name;
  final int duracion;
  final String descripcion;
  final String imgSource;
  final List<String> muscles;
  final int repetitions;

  Exercise.fromJSON(String id, Map<String, dynamic> parsedJson)
      : id = id,
        name = parsedJson['nombre'],
        duracion = parsedJson['tiempo'],
        descripcion = parsedJson['descripcion'],
        imgSource = parsedJson['img'],
        repetitions = parsedJson['repeticiones'],
        muscles = List<String>.from(parsedJson['musculos']);
}
