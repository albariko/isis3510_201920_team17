import 'package:cloud_firestore/cloud_firestore.dart';

class User {
  String id;
  int estatura;
  DateTime fechaCirujia;
  DateTime fechaNacimiento;
  String medio;
  String nombre;
  String rh;
  double peso;
  int tiempoDiario;
  String img;
  String chat;
  List<String> imagenes;
  List<double> examenesFlexion;
  List<double> examenesExtension;
  User(
      {this.id,
      this.estatura,
      this.fechaCirujia,
      this.fechaNacimiento,
      this.img,
      this.medio,
      this.nombre,
      this.peso,
      this.rh,
      this.chat,
      this.imagenes,
      this.examenesFlexion,
      this.examenesExtension});

  int get edad =>
      DateTime.fromMicrosecondsSinceEpoch(
              DateTime.now().difference(fechaNacimiento).inMilliseconds)
          .year -
      1970;

  User.fromJson(String idp, Map<String, dynamic> json) {
    id = idp;
    nombre = json['nombre'].toString();
    fechaCirujia = json['fechaCirujia']?.toDate();
    fechaNacimiento = json['fechaNacimiento']?.toDate();
    medio = json['medioAtencion'];
    peso = double.parse(json['peso'].toString());
    rh = json['rh'];
    tiempoDiario = int.parse(json['tiempoDiario'].toString());
    img = json['img'];
    estatura = int.parse(json['estatura'].toString());
    chat = json['chat'];
    imagenes = json['imagenes']==null?null:List<String>.from(json['imagenes']);
    examenesFlexion = json['examenesFlexion']==null?null:List<double>.from(json['examenesFlexion']);
    examenesExtension = json['examenesExtension']==null?null:List<double>.from(json['examenesExtension']);
  }

  Map<String, dynamic> toJson() {
    return {
      'estatura': estatura,
      'fechaCirujia': (fechaCirujia!=null)?Timestamp.fromDate(fechaCirujia):null, 
      'fechaNacimiento':  (fechaCirujia!=null)?Timestamp.fromDate(fechaNacimiento):null,
      'medioAtencion': medio,
      'nombre': nombre,
      'peso': peso,
      'rh': rh,
      'tiempoDiario': tiempoDiario,
      'img': img,
      'imagenes':imagenes,
      'examenesFlexion': examenesFlexion,
      'examenesExtension': examenesExtension
    };
  }
}
