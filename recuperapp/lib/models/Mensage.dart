import 'package:cloud_firestore/cloud_firestore.dart';

import 'User.dart';

class Mensage {
  User user;
  int userIdPos;
  String msg;
  DateTime time;

  Mensage({this.userIdPos, this.msg, this.time, this.user});

  Mensage.fromJson(Map<String, dynamic> json) {
    msg = json['msg'];
    time = json['date']?.toDate();
    userIdPos = json['user'];
  }

  Map<String,dynamic> toJson() => {
        'msg': msg,
        'date': (time != null) ? Timestamp.fromDate(time) : null,
        'user': userIdPos
  };
}
