enum Category { Exam, Exercise, Medicine }

class Task {
  String idTask;
  String name;
  Category category;
  int time;
  String id;
  bool done;

  Task({this.name, this.category, this.time, this.id, this.done});

  Task.fromJson(String pidTask, Map<String, dynamic> json) {
    idTask = pidTask;
    name = json['nombre'];
    time = json['tiempo'];
    id = json['id'];
    done = json['done'];
    category = _getCategory(json['tipo']);
  }

  Map<String, dynamic> toJson() {
    return {
      'nombre': name,
      'tiempo': time,
      'id': id,
      'done': done,
      'tipo': _getCate(category)
    };
  }

  String _getCate(Category cat) {
    switch (cat) {
      case Category.Exercise:
        return 'ejercicio';
      case Category.Exam:
        return 'examen';
      case Category.Medicine:
        return 'medicina';
      default:
        return null;
    }
  }

  Category _getCategory(String s) {
    switch (s) {
      case 'ejercicio':
        return Category.Exercise;
      case 'examen':
        return Category.Exam;
      case 'medicina':
        return Category.Medicine;
    }
    return null;
  }
}
