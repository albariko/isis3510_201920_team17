import 'package:rxdart/rxdart.dart';
import '../resources/providers.dart';
import '../models/models.dart';
import 'blocs.dart';

///
/// Este bloc es entacra del manejo delos datos del usuario
///
class UserBloc extends Bloc {
  /// Este es el objeto del singleton
  static UserBloc _single;

  /// Este es el repositorio encargado de manejar datos del usuario
  final UserProvider _usuarioProvider;

  /// Stream para los datos de un usuario
  final _in = BehaviorSubject<User>();

  /// Stream para manejar los cambios
  final _out = BehaviorSubject<User>();

  /// Constructor public
  factory UserBloc(String userId) {
    if (_single == null) _single = UserBloc._internal(userId);
    return _single;
  }

  /// Constructor privado
  UserBloc._internal(String userId) : _usuarioProvider = UserProvider(userId) {
    _init();
    _in.stream.forEach((User usuario) => _usuarioProvider.setUsuario(usuario));
  }

  /// Llenar el stream con el valor inicial
  _init() async {
    _out.sink.add(await _usuarioProvider.getUsuario());
  }

  /// Cambiar los datos del usuario
  setData(User usuario) => _in.sink.add(usuario);

  /// Stream con la informacion del usuario
  Observable<User> get infoUsuario => _out.stream;

  /// Cerrar los sterams
  @override
  void dispose() {
    _in.close();
    _out.close();
  }
}
