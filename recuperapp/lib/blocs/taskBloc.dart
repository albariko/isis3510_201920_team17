import 'package:rxdart/rxdart.dart';
import '../resources/providers.dart';
import '../models/models.dart';
import 'blocs.dart';

///
/// Este bloque se encarga de manejar las tareas diarias
///  de el usuario
///
class TaskBloc extends Bloc {
  /// Este es el objeto singleton
  static TaskBloc _single;

  /// Manejador de tareas
  final TaskProvider _tareasProvider;

  /// Lista de tareas (es paratener un estado local en lugar de multiples consultas a firebase)
  List<Task> _a = [];

  /// Stream de salida para las tareas
  final _out = BehaviorSubject<List<Task>>();

  /// Stream para agrear un ejercicio como tarea
  final _inExrecise = BehaviorSubject<Exercise>();

  /// Stream para marcar una tarea comofinalizada
  final _mark = BehaviorSubject<String>();

  /// Stream para filtrar las tareas
  final _filter = BehaviorSubject<Category>();

  /// Constructor publico
  factory TaskBloc(String id) {
    if (_single == null) _single = TaskBloc._internal(id);
    return _single;
  }

  /// Constructor privado
  TaskBloc._internal(String userId) : _tareasProvider = TaskProvider(userId) {
    _init();
    _inExrecise.listen((Exercise e) {
      Task b = Task(
          name: e.name,
          time: e.duracion,
          id: e.id,
          done: false,
          category: Category.Exercise);
      _a.add(b);
      _out.sink.add(_a);
      _tareasProvider.addTask(b);
    });
    _mark.listen((String idTask) {
      _a.removeWhere((Task t) => t.idTask == idTask);
      _out.sink.add(_a);
      _tareasProvider.markTask(idTask);
    });
    _filter.listen((Category cat) {
      if (cat == null) {
        _out.sink.add(_a);
      } else {
        var sub = _a.where((Task t) => t.category == cat).toList();
        _out.sink.add(sub);
      }
    });
  }

  /// Inicialiazr la lista de tareas
  _init() async {
    _a = await _tareasProvider.dailyTask();
    _a.addAll([
      Task(name: 'Paracetamol', category: Category.Medicine),
      Task(name: 'Flexión de rodilla', category: Category.Exam, time: 10),
    ]);
    _out.sink.add(_a);
  }

  /// Agregar el ejercio a la lista de tareas
  addExercise(Exercise e) => _inExrecise.sink.add(e);

  /// Filtrar la lista de tareas por categoria
  filter(Category cat) => _filter.sink.add(cat);

  /// Marcar la tarea con el ID como realizada
  markAsDone(String taskId) => _mark.sink.add(taskId);

  /// Stream con la lista de tareas
  Observable<List<Task>> get tasks => _out.stream;

  /// Cerrar los streams
  @override
  void dispose() {
    _out.close();
    _inExrecise.close();
    _mark.close();
    _filter.close();
  }
}
