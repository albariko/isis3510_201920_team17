import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import '../resources/providers.dart';
import 'blocs.dart';

///
/// Este Bloc es el encargado de manejar la autenticacion
/// la aplicacion necesita este Bloc antes del loggin para
/// poder revisar si se loggea o no.
///
class AuthBloc extends Bloc {
  /// Este es el objeto del singleton
  static AuthBloc _single = AuthBloc._internal();

  /// Este es el repositorio encargado de la autenticacion
  final AuthProvider repo = AuthProvider();

  /// Entrada de usuarios
  final _in = BehaviorSubject<UserAut>();

  /// Etream para logearse, sube el user Id cuando la es correcta
  final _out = BehaviorSubject<String>();

  /// Para subir errores de email
  final _emailError = BehaviorSubject<String>();

  /// Para subir errores con la contraseña
  final _passError = BehaviorSubject<String>();

  /// Este Stream es para manejar el loggin correcto
  Observable<String> get answer => _out.stream;

  /// Este Stream es para errores de email
  Observable<String> get emailError => _emailError.stream;

  /// Este Stream es para errores de contraseña
  Observable<String> get passWordError => _passError.stream;

  /// Constructor publico
  factory AuthBloc() => _single;

  /// Constructor privado
  AuthBloc._internal() {
    _in.stream.listen((UserAut userAut) async {
      if (userAut.email == null || userAut.email.isEmpty) {
        _emailError.sink.add('Este campo es requerido');
      } else if (!_isEmail(userAut.email))
        _emailError.sink.add('Formato de email invalido');
      else if (userAut.pass == null || userAut.pass.isEmpty) {
        _emailError.sink.add(null);
        _passError.sink.add('Este campo es requerido');
      } else {
        try {
          LoginStatus a =
              await repo.authenticateUser(userAut.email, userAut.pass);
          switch (a) {
            case LoginStatus.Correct:
              _out.sink.add(repo.getUserId());
              _emailError.sink.add(null);
              _passError.sink.add(null);
              break;
            case LoginStatus.IncorrectUserName:
              _emailError.sink.add('El email no esta registrado');
              _passError.sink.add(null);
              break;
            case LoginStatus.IncorrectUserPassword:
              _passError.sink.add('La contraseña es incorrecta');
              _emailError.sink.add(null);
              break;
            case LoginStatus.NonEmail:
              _emailError.sink.add('El email es incorrecto');
              _passError.sink.add(null);
              break;
            case LoginStatus.NetworkError:
              _emailError.sink.add('Falla de red');
              _passError.sink.add('Falla de red');
              break;
          }
        } catch (e) {
          _emailError.sink.add(e.message);
          _passError.sink.add(e.message);
        }
      }
    });
  }

  /// El metodo para subir al Stream el usuario y contraseña
  loggin(UserAut aut) => _in.sink.add(aut);

  /// Este metodo es para revisar que si es un email
  bool _isEmail(String email) {
    return email.contains('@') && !email.contains(" ");
  }

  /// Cerrar los Streams
  @override
  void dispose() {
    _in.close();
    _out.close();
    _emailError.close();
    _passError.close();
  }
}

///
/// Esta clase es para subir el email y contraseña en un solo Objeto
///
class UserAut {
  UserAut({@required this.email, @required this.pass});
  final String email;
  final String pass;
}
