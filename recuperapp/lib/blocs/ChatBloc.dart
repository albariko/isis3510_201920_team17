import 'package:rxdart/rxdart.dart';
import '../resources/providers.dart';
import '../models/models.dart';
import 'blocs.dart';

///
/// Este bloque se encarga de manejar un chat del Usuario
///
class ChatBloc extends Bloc {
  /// Este es el objeto singleton
  static ChatBloc _single;

  /// proveedor del chat
  ChatProvider chatProvider;

  /// mensajes
  List<Mensage> msgs = [];

  /// setram para los mensajes
  final _chat = BehaviorSubject<List<Mensage>>();

  /// Stream para postear
  final _post = BehaviorSubject<Mensage>();

  /// Constructor publico
  factory ChatBloc(User u,String idChat){
    if(_single== null)
    _single = ChatBloc._internal(u,idChat);
    return _single;
  }

  /// Constructor privado
  ChatBloc._internal(User u,String idChat){
    chatProvider = ChatProvider(u, idChat);
    chatProvider.listener((Mensage nuevo){
       msgs.insert(0,nuevo);
       _chat.sink.add(msgs);
    });
    _init();
    _post.listen((Mensage m){
      chatProvider.post(m);
    });
  }

  /// cargar los mensajes de firabase
  _init() async{
    //msgs = await chatProvider.getChat();
    _chat.sink.add(msgs);
  }

  /// Stream con la lista de mensajes
  Observable<List<Mensage>> get mensajes => _chat.stream;

  /// Marcar la tarea con el ID como realizada
  send(Mensage m) => _post.sink.add(m);


  @override
  void dispose() {
   _chat.close();
   _post.close();
  }

}