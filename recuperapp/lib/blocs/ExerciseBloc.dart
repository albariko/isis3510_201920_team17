import 'package:rxdart/rxdart.dart';
import '../resources/providers.dart';
import '../models/models.dart';
import 'blocs.dart';

///
/// Este Bloc es el encargado de manejar los ejercicios
/// y la busqueda por musculos
///
class ExerciseBlock extends Bloc {
  /// Este es el objeto del singleton
  static ExerciseBlock _single = ExerciseBlock._internal();

  /// Este es el repositorio encargado de proveer ejercios
  final ExerciseProvider _ejercicioProvider = ExerciseProvider();

  /// Enrada del musculo para filtrar
  final _in = BehaviorSubject<String>();

  /// Stream para poner los musculos posibles
  final _musculos = BehaviorSubject<List<String>>();

  /// Stream con la lista de los musculos filtrada
  final _out = BehaviorSubject<List<Exercise>>();

  /// Constructor publico
  factory ExerciseBlock() => _single;

  // Constructor Privado
  ExerciseBlock._internal() {
    _init();
    _in.listen((String string) async {
      _out.sink
          .add(await _ejercicioProvider.getFilteredExercises(musculo: string));
    });
  }

  /// Llena los streams con los valores iniciales
  _init() async {
    _musculos.sink.add(await _ejercicioProvider.getMusculos());
    _out.sink.add(await _ejercicioProvider.getExercises());
  }

  /// Agrega el stream para filtrar por musculo
  filter(String s) => _in.sink.add(s);

  /// Este Stream es para la lista de musculos
  Observable<List<String>> get muculos => _musculos.stream;

  /// Este estream es para la lista de ejercicios
  Observable<List<Exercise>> get ejercicios => _out.stream;

  /// Cerrar los Streams
  @override
  void dispose() {
    _in.close();
    _musculos.close();
    _out.close();
  }
}
