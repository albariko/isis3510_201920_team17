import 'package:flutter/material.dart';
import 'package:recuperapp/ui/LoginView/LoginView.dart';
import 'blocs/blocs.dart';
import 'package:flutter/services.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);
    return MaterialApp(
      title: 'Flutter Demo',
      theme: _light(context),
      home: BlocProvider(bloc: AuthBloc(), child: LoginView()),
      debugShowCheckedModeBanner: false,
    );
  }

  ThemeData _light(BuildContext context) => ThemeData(
      appBarTheme: AppBarTheme(
          color: const Color(0XFF023555),
          elevation: 5,
          brightness: Brightness.dark),
      primaryColor: const Color(0XFF023555),
      primaryColorDark: const Color(0XFF022545),
      buttonColor: const Color(0XFF023555),
      accentColor: const Color(0XFF187594) ,
      //fontFamily: 'Playfair',
  );

  ThemeData dark() => ThemeData();
}
