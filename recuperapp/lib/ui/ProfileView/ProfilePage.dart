import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import '../../models/models.dart';
import '../../blocs/blocs.dart';

class ProfilePage extends StatefulWidget {
  @override
  MapScreenState createState() => MapScreenState();
}

class MapScreenState extends State<ProfilePage>
    with SingleTickerProviderStateMixin {
  bool _status = true;
  final _nombreControler = TextEditingController();
  final _estaturaControler = TextEditingController();
  final _pesoControler = TextEditingController();
  final _rhControler = TextEditingController();
  final _nacimientoControler = TextEditingController();
  final _cirujiaControler = TextEditingController();
  var fechaNacimientotext = TextEditingController();
  var fechaCirugiatext = TextEditingController();

  Observable<User> stream;
  UserBloc _userBloc;
  User current;

  bool hasConection;

  @override
  initState() {
    super.initState();
    hasConection = ConnectionStatus.getInstance().hasConnection;
    ConnectionStatus.getInstance().connectionChange.listen((bool con) {
      setState(() {
        hasConection = con;
      });
    });
    _userBloc = BlocProvider.of<UserBloc>(context);
    stream = _userBloc.infoUsuario;
    stream.listen((User usuario) {
      setState(() {
        current = usuario;
        setControlers();
      });
    });
  }

  void setControlers() {
    _nombreControler.text = current.nombre;
    _estaturaControler.text = '${current.estatura}';
    var a = current.peso.round();
    _pesoControler.text = a.toString();
    _rhControler.text = current.rh;
    _nacimientoControler.text =
        '${current.fechaNacimiento.day}/${current.fechaNacimiento.month}/${current.fechaNacimiento.year}';
    _cirujiaControler.text =
        '${current.fechaCirujia.day}/${current.fechaCirujia.month}/${current.fechaCirujia.year}';
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    List<double> margin = [(1 / 30) * size.width, (1 / 30) * size.height];
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          actions: (!hasConection)
              ? [
                  Padding(
                      padding: EdgeInsets.only(right: margin[0]),
                      child: Icon(Icons.signal_wifi_off, color: Colors.orange[800]))
                ]
              : null,
          title: Text('Perfil'),
        ),
        body: ListView(children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: (1 / 60) * size.height),
            child: Stack(fit: StackFit.loose, children: <Widget>[
              Center(
                  child: Container(
                      width: (5 / 12) * size.width,
                      height: (5 / 12) * size.width,
                      decoration: BoxDecoration(
                        image: (current?.img != null)
                            ? DecorationImage(
                                image: NetworkImage(current.img),
                                fit: BoxFit.fill)
                            : null,
                        color: Colors.blueGrey,
                        shape: BoxShape.circle,
                      ),
                      child: (current?.img == null)
                          ? Icon(Icons.person,
                              size: (3 / 4) * (5 / 12) * size.width,
                              color: Colors.white)
                          : Container())),
              Align(
                  alignment: Alignment.center,
                  child: Padding(
                      padding: EdgeInsets.only(
                          top: (3 / 12) * size.width,
                          right: (4 / 12) * size.width),
                      child: CircleAvatar(
                        backgroundColor: Theme.of(context).primaryColor,
                        radius: (1 / 14) * size.width,
                        child: Icon(
                          Icons.camera_alt,
                          color: Colors.white,
                        ),
                      )))
            ]),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: (1 / 20) * size.width),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Text(
                      'Información personal',
                      style: TextStyle(
                          fontSize: 22.0, fontWeight: FontWeight.bold),
                    ),
                    _status ? _getEditIcon(context) : new Container(),
                  ],
                ),
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: (1 / 40) * size.width),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Nombre',
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.bold),
                      ),
                      Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: (1 / 30) * size.width),
                          child: TextField(
                            controller: _nombreControler,
                            maxLength: 30,
                            enabled: !_status,
                            autofocus: false,
                          )),
                      Text(
                        'Estatura en cm',
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.bold),
                      ),
                      Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: (1 / 30) * size.width),
                          child: TextField(
                            controller: _estaturaControler,
                            maxLength: 3,
                            keyboardType: TextInputType.number,
                            enabled: !_status,
                            autofocus: false,
                          )),
                      Text(
                        'Peso en kg',
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.bold),
                      ),
                      Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: (1 / 30) * size.width),
                          child: TextField(
                            controller: _pesoControler,
                            maxLength: 4,
                            keyboardType: TextInputType.number,
                            enabled: !_status,
                            autofocus: false,
                          )),
                      Text(
                        'Grupo sanguineo',
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.bold),
                      ),
                      Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: (1 / 30) * size.width),
                          child: TextField(
                            controller: _rhControler,
                            maxLength: 2,
                            keyboardType: TextInputType.text,
                            enabled: !_status,
                            autofocus: false,
                          )),
                      Text(
                        'Fecha de Nacimiento',
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.bold),
                      ),
                      FlatButton(
                          onPressed: _status
                              ? null
                              : () {
                                  DatePicker.showDatePicker(context,
                                      showTitleActions: true,
                                      minTime: DateTime(1940, 1, 1),
                                      maxTime: DateTime.now(),
                                      onChanged: (date) {
                                    print('change $date');
                                  }, onConfirm: (date) {
                                    fechaNacimientotext.text = '$date';
                                    fechaNacimientotext.text =
                                        fechaNacimientotext.text
                                            .split(" ")[0]
                                            .replaceAll("-", "/");

                                    _nacimientoControler.text =
                                        fechaNacimientotext.text;
                                    print('confirm $date');
                                  },
                                      currentTime: DateTime.now(),
                                      locale: LocaleType.es);
                                },
                          child: Text(
                            'Cambiar fecha de nacimiento',
                            style: TextStyle(color: Colors.blue),
                          )),
                      Text(
                        _nacimientoControler.text,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Text(
                        'Fecha de Cirugía',
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.bold),
                      ),
                      FlatButton(
                          onPressed: _status
                              ? null
                              : () {
                                  DatePicker.showDatePicker(context,
                                      showTitleActions: true,
                                      minTime: DateTime(1940, 1, 1),
                                      maxTime: DateTime.now(),
                                      onChanged: (date) {
                                    print('change $date');
                                  }, onConfirm: (date) {
                                    fechaCirugiatext.text = '$date';
                                    _cirujiaControler.text = fechaCirugiatext
                                        .text
                                        .split(" ")[0]
                                        .replaceAll("-", "/");
                                    print('confirm $date');
                                  },
                                      currentTime: DateTime.now(),
                                      locale: LocaleType.es);
                                },
                          child: Text(
                            'Cambiar fecha de cirugía',
                            style: TextStyle(color: Colors.blue),
                          )),
                      Text(
                        _cirujiaControler.text,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      !_status ? _getActionButtons(_userBloc) : new Container(),
                    ],
                  ),
                )
              ],
            ),
          )
        ]));
  }

  Widget _getActionButtons(UserBloc _userBloc) {
    return Padding(
      padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 45.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: Container(
                  child: RaisedButton(
                child: Text("Guardar"),
                textColor: Colors.white,
                color: Colors.green,
                onPressed: () {
                  //CHECK DE NOMBRE
                  if (_nombreControler.text == "" ||
                      (_nombreControler.text
                          .toUpperCase()
                          .replaceAll(new RegExp(r' '), '')
                          .contains(new RegExp('[^A-Z]')))) {
                    Flushbar(
                      margin: EdgeInsets.all(8),
                      borderRadius: 8,
                      message: "Debes ingresar tu nombre",
                      icon: Icon(
                        Icons.info_outline,
                        size: 28.0,
                        color: Colors.yellow[300],
                      ),
                      duration: Duration(seconds: 3),
                      leftBarIndicatorColor: Colors.yellow[300],
                    )..show(context);
                    return;
                  }
                  if (current.nombre != _nombreControler.text) {
                    current.nombre = _nombreControler.text;
                  }

                  //CHECK DE ESTATURA
                  if (_estaturaControler.text.contains(new RegExp('[^0-9]')) ||
                      int.parse(_estaturaControler.text) < 60 ||
                      int.parse(_estaturaControler.text) > 272) {
                    Flushbar(
                      margin: EdgeInsets.all(8),
                      borderRadius: 8,
                      message: "Debes ingresar tu estatura en cm",
                      icon: Icon(
                        Icons.info_outline,
                        size: 28.0,
                        color: Colors.yellow[300],
                      ),
                      duration: Duration(seconds: 3),
                      leftBarIndicatorColor: Colors.yellow[300],
                    )..show(context);
                    return;
                  }
                  if (current.estatura != int.parse(_estaturaControler.text))
                    current.estatura = int.parse(_estaturaControler.text);

                  //CHECK DE PESO
                  var pesoTemp =_pesoControler.text;
                  pesoTemp.replaceAll(".", "");
                  pesoTemp.replaceAll(",", "");
                  if (pesoTemp.contains(new RegExp('[^0-9]')) ||
                      int.parse(pesoTemp) < 20 ||
                      int.parse(pesoTemp) > 600) {
                    Flushbar(
                      margin: EdgeInsets.all(8),
                      borderRadius: 8,
                      message: "Debes ingresar tu peso en kg, solo números",
                      icon: Icon(
                        Icons.info_outline,
                        size: 28.0,
                        color: Colors.yellow[300],
                      ),
                      duration: Duration(seconds: 3),
                      leftBarIndicatorColor: Colors.yellow[300],
                    )..show(context);
                    return;
                  }
                  if (current.peso != double.parse(_pesoControler.text))
                    current.peso = double.parse(_pesoControler.text);
                  //CHECK DE RH
                  if ((_rhControler.text.contains('A') ||
                          _rhControler.text.contains('a') ||
                          _rhControler.text.contains('B') ||
                          _rhControler.text.contains('b') ||
                          _rhControler.text.contains('o') ||
                          _rhControler.text.contains('O')) &&
                      (_rhControler.text.contains('+') ||
                          _rhControler.text.contains('-'))) {
                    if (current.rh != _rhControler.text) {
                      current.rh = _rhControler.text;
                    }
                  } else {
                    Flushbar(
                      margin: EdgeInsets.all(8),
                      borderRadius: 8,
                      message: "Debes ingresar tu grupo sanguineo",
                      icon: Icon(
                        Icons.info_outline,
                        size: 28.0,
                        color: Colors.yellow[300],
                      ),
                      duration: Duration(seconds: 3),
                      leftBarIndicatorColor: Colors.yellow[300],
                    )..show(context);
                    return;
                  }

                  //CHECK DE FECHA
                  var fecha = _nacimientoControler.text.split('/');
                  var fecha2 = _cirujiaControler.text.split('/');
                  if (_nacimientoControler.text != null) if ('${current.fechaNacimiento.day}/${current.fechaNacimiento.month}/${current.fechaNacimiento.year}' !=
                      _nacimientoControler.text) {
                    DateTime a;
                    if (_nacimientoControler.text != null) {
                      a = DateTime.parse(
                          _nacimientoControler.text.replaceAll("/", "-"));
                    } else {
                      a = current.fechaNacimiento;
                    }

                    DateTime b;
                    if (_cirujiaControler.text != null) {
                      b = DateTime.parse(
                          _cirujiaControler.text.replaceAll("/", "-"));
                    } else {
                      b = current.fechaCirujia;
                    }

                    var difference = b.difference(a).inDays;

                    if (difference > 0) {
                      print("ee");
                      current.fechaNacimiento = DateTime(int.parse(fecha[0]),
                          int.parse(fecha[1]), int.parse(fecha[2]));
                    } else {
                      Flushbar(
                        margin: EdgeInsets.all(8),
                        borderRadius: 8,
                        message: "Revisa tu fecha de nacimiento y de cirugía",
                        icon: Icon(
                          Icons.info_outline,
                          size: 28.0,
                          color: Colors.yellow[300],
                        ),
                        duration: Duration(seconds: 3),
                        leftBarIndicatorColor: Colors.yellow[300],
                      )..show(context);
                      return;
                    }
                  }

                  if ('${current.fechaCirujia.day}/${current.fechaCirujia.month}/${current.fechaCirujia.year}' !=
                      _cirujiaControler.text) {
                    DateTime a;
                    if (fechaNacimientotext != null) {
                      print("aaa");
                      a = DateTime.parse(
                          _nacimientoControler.text.replaceAll("/", "-"));
                    } else {
                      a = current.fechaNacimiento;
                    }

                    DateTime b;
                    if (_cirujiaControler.text != null) {
                      b = DateTime.parse(
                          _cirujiaControler.text.replaceAll("/", "-"));
                    } else {
                      b = current.fechaCirujia;
                    }

                    var difference = b.difference(a).inDays;

                    if (difference > 0) {
                      print("ee");
                      current.fechaCirujia = DateTime(int.parse(fecha2[0]),
                          int.parse(fecha2[1]), int.parse(fecha2[2]));
                    } else {
                      Flushbar(
                        margin: EdgeInsets.all(8),
                        borderRadius: 8,
                        message: "Revisa tu fecha de nacimiento y de cirugía",
                        icon: Icon(
                          Icons.info_outline,
                          size: 28.0,
                          color: Colors.yellow[300],
                        ),
                        duration: Duration(seconds: 3),
                        leftBarIndicatorColor: Colors.yellow[300],
                      )..show(context);
                      return;
                    }
                  }

                  _userBloc.setData(current);
                  setState(() {
                    _status = true;
                  });
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0)),
              )),
            ),
            flex: 2,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: Container(
                  child: RaisedButton(
                child: Text("Cancelar"),
                textColor: Colors.white,
                color: Colors.red,
                onPressed: () {
                  setState(() {
                    setControlers();
                    _status = true;
                  });
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0)),
              )),
            ),
            flex: 2,
          ),
        ],
      ),
    );
  }

  Widget _getEditIcon(BuildContext context) {
    return GestureDetector(
      child: CircleAvatar(
        backgroundColor:
            hasConection ? Theme.of(context).primaryColor : Colors.grey,
        radius: 30.0,
        child: Icon(
          Icons.edit,
          color: Colors.white,
          size: 20.0,
        ),
      ),
      onTap: () {
        if (hasConection) {
          setState(() {
            _status = false;
          });
        }
      },
    );
  }
}
