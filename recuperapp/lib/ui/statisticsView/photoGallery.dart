import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:recuperapp/blocs/blocs.dart' ;
import 'package:recuperapp/models/User.dart';




class GalleryPage extends StatelessWidget {
  final imageList = [
    'https://resocoder.com/wp-content/uploads/2019/04/thumbnail-2.png',
    'https://resocoder.com/wp-content/uploads/2019/04/thumbnail-1.png',
    'https://resocoder.com/wp-content/uploads/2019/01/thumbnail.png',
  ];

  @override
  Widget build(BuildContext context) {
    UserBloc user = BlocProvider.of<UserBloc>(context);
    return WillPopScope(
      onWillPop: () {
        return new Future.value(true);
      },
          child: Scaffold(
          appBar: AppBar(
          automaticallyImplyLeading: true,
          centerTitle: true,
          title: Text('Galería'),
          leading: IconButton(icon:Icon(Icons.arrow_back_ios),
            onPressed:() => Navigator.pop(context, false),
          )
        ),
        // Implemented with a PageView, simpler than setting it up yourself
        // You can either specify images directly or by using a builder as in this tutorial
        body: StreamBuilder(stream: user.infoUsuario, builder: (BuildContext context,AsyncSnapshot<User> snap)=>(snap.hasData)?
        
        PhotoViewGallery.builder(
          itemCount: snap.data.imagenes.length,
          builder: (context, index) {
            return PhotoViewGalleryPageOptions(
              imageProvider: NetworkImage(
                snap.data.imagenes[index],
              ),
              // Contained = the smallest possible size to fit one dimension of the screen
              minScale: PhotoViewComputedScale.contained * 0.8,
              // Covered = the smallest possible size to fit the whole screen
              maxScale: PhotoViewComputedScale.covered * 2,
            );
          },
          scrollPhysics: BouncingScrollPhysics(),
          // Set the background color to the "classic white"
          backgroundDecoration: BoxDecoration(
            color: Theme.of(context).canvasColor,
          ),
          loadingChild: Center(
            child: CircularProgressIndicator(),
          ),
        ):Center(child: CircularProgressIndicator())),
      ),
    );
  }
}