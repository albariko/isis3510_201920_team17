import 'dart:io';

import 'package:date_format/date_format.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:image_picker/image_picker.dart';
import 'package:recuperapp/blocs/UserBlock.dart';
import 'package:recuperapp/ui/statisticsView/photoGallery.dart';
import 'package:rxdart/rxdart.dart';

import '../../blocs/blocs.dart';
import '../../models/User.dart';


class StatsView extends StatefulWidget {
  

  final String title = "Estadísticas";

  @override
  _StatsView createState() => _StatsView();
}

class _StatsView extends State<StatsView> {
  //var data = [0.0, 1.0, 1.5, 2.0, 0.0, 0.0, -0.5, -1.0, -0.5, 0.0, 0.0];
  var data1 = [0.0,-2.0,3.5,-2.0,0.5,0.7,0.8,1.0,2.0,3.0,3.2];
  Observable<User> stream;
  UserBloc _userBloc;
  User current;
  File image;

  Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to exit an App'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: new Text('Si'),
          ),
        ],
      ),
    ) ?? false;
  }

  @override
  void initState() {
    super.initState();
    _userBloc = BlocProvider.of<UserBloc>(context);
    stream = _userBloc.infoUsuario;
    stream.listen((User usuario) {
      setState(() {
        current = usuario;
      });
    });
  }

  Future getImage() async {
    var im = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      image = im;
    });

    var now = formatDate(new DateTime.now(), [yyyy,'-', mm ,'-',dd]);
    var fullImageName =  'images/${current.id}-$now'+'.jpg';

    final StorageReference ref = FirebaseStorage(storageBucket: 'gs://recuperapp-8596a.appspot.com/').ref().child(fullImageName);
    final StorageUploadTask task = ref.putFile(image);
    var fullPathImage =  await (await task.onComplete).ref.getDownloadURL();
    String url = fullPathImage.toString();
    setState((){
      current.imagenes.add(url);
    });
    print("el nombre del usuario actual es: "+current.nombre.toString());
    print(current.toJson());
    _userBloc.setData(current);
    image = null;
  }


  Material myTextItems(String title, String subtitle, bool corazon){
    IconData icono;
    if (corazon) {
      icono = Icons.favorite;
    } else {
      icono = Icons.directions_run;
    }
    return Material(
      color: Colors.white,
      elevation: 14.0,
      borderRadius: BorderRadius.circular(24.0),
      shadowColor: Color(0x802196F3),
      child: Center(
        child:Padding(
          padding: EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment:MainAxisAlignment.center,
            children: <Widget>[
              Column(
                mainAxisAlignment:MainAxisAlignment.center,
               children: <Widget>[

                  Padding(
                   padding: EdgeInsets.all(8.0),
                      child:Text(title, textAlign: TextAlign.center , style:TextStyle(
                        
                        fontSize: 20.0,
                        
                        color: Colors.blueAccent,
                      ),),
                    ),

                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child:Text(subtitle,style:TextStyle(
                      fontSize: 30.0,
                    ),),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Icon(icono),
                  ),

               ],
              ),
            ],
          ),
        ),
      ),
    );
  }


  Material mychart1Items(String title, String priceVal,String subtitle, List<double> data) {
    return Material(
      color: Colors.white,
      elevation: 2.0,
      borderRadius: BorderRadius.circular(24.0),
      child: Center(
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(1.0),
                    child: Text(title,textAlign: TextAlign.center, style: TextStyle(
                      fontSize: 20.0,
                      color: Colors.blueAccent,
                    ),),
                  ),
                  Padding(
                    padding: EdgeInsets.all(1.0),
                    child: Text(priceVal, style: TextStyle(
                      fontSize: 30.0,
                    ),),
                  ),
                  Padding(
                    padding: EdgeInsets.all(1.0),
                    child: Text(subtitle, style: TextStyle(
                      fontSize: 20.0,
                      color: Colors.blueGrey,
                    ),),
                  ),
                  Padding(
                    padding: EdgeInsets.all(1.0),
                    child: new Sparkline(
                      data: data,
                      lineColor: Color(0xffff6101),
                      pointsMode: PointsMode.all,
                      pointSize: 8.0,
                    ),
                  ),

                ],
              ),
            ],
          ),
        ),
      ),
    );
  }


  Material mychart2Items(String title, String priceVal,String subtitle) {
    return Material(
      color: Colors.white,
      elevation: 14.0,
      borderRadius: BorderRadius.circular(24.0),
      shadowColor: Color(0x802196F3),
      child: Center(
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  Padding(
                    padding: EdgeInsets.all(1.0),
                    child: Text(title, textAlign: TextAlign.center, style: TextStyle(
                      fontSize: 20.0,
                      color: Colors.blueAccent,
                    ),),
                  ),

                  Padding(
                    padding: EdgeInsets.all(1.0),
                    child: Text(priceVal, style: TextStyle(
                      fontSize: 30.0,
                    ),),
                  ),
                  Padding(
                    padding: EdgeInsets.all(1.0),
                    child: Text(subtitle, style: TextStyle(
                      fontSize: 20.0,
                      color: Colors.blueGrey,
                    ),),
                  ),

                  Padding(
                    padding: EdgeInsets.all(1.0),
                    child: new Sparkline(
                      data: data1,
                      fillMode: FillMode.below,
                      fillGradient: new LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Colors.amber[800], Colors.amber[200]],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    UserBloc user = BlocProvider.of<UserBloc>(context);
    Size size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: _onWillPop,
          child: Scaffold(
        appBar: AppBar(
          title: Text(widget.title, textAlign: TextAlign.center),
          actions: <Widget>[
          ],
        ),
        body:Container(
            color:Color(0xffE5E5E5),
            child:StaggeredGridView.count(
              crossAxisCount: 4,
             crossAxisSpacing: 8.0,
            mainAxisSpacing: 8.0,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: StreamBuilder(stream: user.infoUsuario, builder: (BuildContext context,AsyncSnapshot<User> snap)=>(snap.hasData)? 
              mychart1Items("Progreso de la recuperacion","60%","Evolución último mes:",snap.data.examenesFlexion):Center(child: CircularProgressIndicator())),
            ),
            Padding(
              padding: const EdgeInsets.only(right:8.0),
              child: StreamBuilder(stream: user.infoUsuario, builder: (BuildContext context,AsyncSnapshot<User> snap)=>(snap.hasData)?
              myTextItems("Flexión",snap.data.examenesFlexion[snap.data.examenesFlexion.length-1].toString()+"°",false):Center(child: CircularProgressIndicator())),
            ),
            Padding(
              padding: const EdgeInsets.only(right:8.0),
              child: StreamBuilder(stream: user.infoUsuario, builder: (BuildContext context,AsyncSnapshot<User> snap)=>(snap.hasData)?
              myTextItems("Extensión",snap.data.examenesExtension[snap.data.examenesExtension.length-1].toString()+"°",false):Center(child: CircularProgressIndicator())),
            ),
            GestureDetector(
              onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => GalleryPage()),);
                },
              child:
              StreamBuilder(stream: user.infoUsuario, builder: (BuildContext context,AsyncSnapshot<User> snap)=>(snap.hasData)?
            Container(
              padding: const EdgeInsets.only(left:8.0),
              width: size.height/3,
              height: size.height/5,
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(24.0),
                boxShadow: [new BoxShadow(
                color: Colors.black,
                blurRadius: 10.0,
                ),],
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: NetworkImage(
                    snap.data.imagenes[snap.data.imagenes.length-1])
                          )
                )
              ):Center(child: CircularProgressIndicator()))
              ),
          RaisedButton(
            onPressed: () { Navigator.push(context, MaterialPageRoute(builder: (context) => GalleryPage()),);},
            textColor: Colors.white,
            padding: const EdgeInsets.all(0.0),
            child: Container(
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color>[
                    Color(0xFF0D47A1),
                    Color(0xFF1976D2),
                    Color(0xFF42A5F5),
                  ],
                ),
              ),
              padding: const EdgeInsets.all(3.0),
              child: const Text(
                'Ver Galería',
                style: TextStyle(fontSize: 20)
              ),
            ),
          ),
          RaisedButton(
            onPressed: getImage,
            textColor: Colors.white,
            child: Container(
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color>[
                    Color(0xFF0D47A1),
                    Color(0xFF1976D2),
                    Color(0xFF42A5F5),
                  ],
                ),
              ),
              child: const Text(
                'Tomar Nueva',
                style: TextStyle(fontSize: 20)
              ),
            ),
          ),
          ],
          staggeredTiles: [
            StaggeredTile.extent(4, size.height/3.5),
            StaggeredTile.extent(2, size.height/3.1),
            StaggeredTile.extent(2, size.height/3.1),
            StaggeredTile.extent(2, size.height/5.5),
            StaggeredTile.extent(2, size.height/12),
            StaggeredTile.extent(2, size.height/12),
          ],
        ),
        ),
      ),
    );
  }
}