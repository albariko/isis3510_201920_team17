import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:recuperapp/blocs/blocs.dart';
import 'package:recuperapp/ui/ExamsView/examenExtension.dart';

import 'examenFlexion.dart';

class ExamsView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ExamsViewState();
}

class _ExamsViewState extends State<ExamsView> {
  bool hasConection;

  @override
  initState() {
    super.initState();
    hasConection = ConnectionStatus.getInstance().hasConnection;
    ConnectionStatus.getInstance().connectionChange.listen((bool con) {
      setState(() {
        hasConection = con;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    List<double> margin = [(1 / 30) * size.width, (1 / 30) * size.height];
    return Scaffold(
        appBar: AppBar(
            centerTitle: true,
            title: Text('Exámenes'),
            actions: (!hasConection)
                ? [
                    Padding(
                        padding: EdgeInsets.only(right: margin[0]),
                        child: Icon(Icons.signal_wifi_off, color: Colors.orange[800]))
                  ]
                : null,
            elevation: 10,
            bottom: PreferredSize(
                preferredSize: Size.fromHeight((1 / 4) * size.height),
                child: Image.asset('assets/images/exams.jpg',
                    fit: BoxFit.fitWidth))),
        body: Container(
            margin: EdgeInsets.only(
                top: (1 / 30) * size.width,
                left: (1 / 30) * size.width,
                right: (1 / 30) * size.width),
            child: ListView(
              children: <Widget>[
                CategoryView(
                    name: 'Examen flexión de rodilla',
                    shortDesc:
                        'Para tomarse la prueba de click en este recuadro, debe colocarse el dispositivo para sostener el celular, pulsar el boton tomar medida y luego flexionar la rodilla lo quer mas pueda. cuando escuche el sonido de notificacion la prueba habra acabado y usted podra decidir si mandar el registro a la base de datos',
                    icon: Icons.airline_seat_legroom_extra,
                    action: (BuildContext context) {Navigator.push(context, MaterialPageRoute(builder: (context) => ExamenFlexion()),);}),
                    CategoryView(
                    name: 'Examen de extensión de rodilla',
                    shortDesc:
                        'Para tomarse la prueba de click en este recuadro, debe colocarse el dispositivo para sostener el celular, pulsar el boton tomar medida y luego extender la rodilla lo quer mas pueda. cuando escuche el sonido de notificacion la prueba habra acabado y usted podra decidir si mandar el registro a la base de datos',
                    icon: Icons.airline_seat_legroom_extra,
                    action: (BuildContext context) {Navigator.push(context, MaterialPageRoute(builder: (context) => ExamenExtencion()),);}),
              ],
            )));
  }
}

typedef void DoSomething(BuildContext context);

class CategoryView extends StatelessWidget {
  const CategoryView(
      {@required this.name,
      this.icon: Icons.accessibility_new,
      this.action,
      this.shortDesc: ''});
  final String name;
  final String shortDesc;
  final IconData icon;
  final DoSomething action;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Card(
        elevation: 3,
        child: GestureDetector(
            onTap: () {
              action(context);
            },
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Flexible(
                    flex: 4,
                    child: Container(
                        margin: EdgeInsets.all((1 / 30) * size.width),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(name,
                                style: TextStyle(), textScaleFactor: 1.25),
                            Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: (1 / 20) * size.width),
                              child: Text(shortDesc,
                                  style: TextStyle(color: Colors.grey)),
                            )
                          ],
                        ))),
              ],
            )));
  }
}
