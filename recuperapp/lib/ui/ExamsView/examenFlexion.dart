import 'dart:async';
import 'dart:math';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:recuperapp/blocs/blocs.dart';
import 'package:recuperapp/models/User.dart';
import 'package:rxdart/rxdart.dart';
import 'package:sensors/sensors.dart';

class ExamenFlexion extends StatefulWidget {
  @override
  _ExamenFlexionState createState() => _ExamenFlexionState();
}

class _ExamenFlexionState extends State<ExamenFlexion> {
  Timer _timer;
  AudioPlayer audioPlayer = AudioPlayer();
  List<double> _accelerometerValues;
  List<double> _angleValues;
  //List<double> _userAccelerometerValues;
  //List<double> _gyroscopeValues;
  Observable<User> stream;
  UserBloc _userBloc;
  User current;
  List<StreamSubscription<dynamic>> _streamSubscriptions =
      <StreamSubscription<dynamic>>[];

  @override
  void initState() {
    super.initState();
    _accelerometerValues = <double>[0, 0, 0];
    _angleValues = <double> [0];
    _userBloc = BlocProvider.of<UserBloc>(context);
    stream = _userBloc.infoUsuario;
    stream.listen((User usuario) {
      setState(() {
        current = usuario;
      });
    });
  }

  Future<bool> subirMedida(double medida){
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Estas seguro?'),
        content: new Text('Quieres subir la medidida: '+medida.toString()+'°'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () {
              setState((){
                current.examenesFlexion.add(medida);
              });
              _userBloc.setData(current);
              Navigator.of(context).pop(false);
            },
            child: new Text('Si'),
          ),
        ],
      ),
    ) ?? false;
  }

  playLocal() async {
    int result = await audioPlayer.play('sounds/alarm.mp3', isLocal: true);
  }

  getMeasure() {
  // Imagine that this function is
  // more complex and slow.
  //Accelerometer events
    _streamSubscriptions
        .add(accelerometerEvents.listen((AccelerometerEvent event) {
      setState(() {
        _accelerometerValues = <double>[event.x, event.y, event.z];
      });
    }));

    //Accelerometer events
    _streamSubscriptions
        .add(accelerometerEvents.listen((AccelerometerEvent event) {
      setState(() {
        _angleValues = <double> [atan(event.x/(sqrt(pow(event.y, 2)*sqrt(pow(event.z, 2)))))*(180/pi)];
      });
    }));

    _timer = new Timer(const Duration(milliseconds: 4000), () {
      setState(() {
        for (StreamSubscription<dynamic> sub in _streamSubscriptions) {
          sub.cancel();
        }
        FlutterRingtonePlayer.play(
        android: AndroidSounds.notification,
        ios: IosSounds.glass,
        looping: false,
        volume: 0.5,
        );
        FlutterRingtonePlayer.play(
        android: AndroidSounds.notification,
        ios: IosSounds.glass,
        looping: false,
        volume: 0.5,
        );
        
      });
    });
    
    
    }

  @override
  void dispose() {
    for (StreamSubscription<dynamic> sub in _streamSubscriptions) {
      sub.cancel();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final List<String> accelerometer =
        _accelerometerValues?.map((double v) => v.toStringAsFixed(1))?.toList();
    final List<String> angle =
        _angleValues?.map((double v) => v.toStringAsFixed(1))?.toList();   
    //final List<String> gyroscope =
    //    _gyroscopeValues?.map((double v) => v.toStringAsFixed(1))?.toList();
    //final List<String> userAccelerometer = _userAccelerometerValues
    //    ?.map((double v) => v.toStringAsFixed(1))?.toList();

    final double angulo = 90-double.parse(angle[0].replaceAll("[", "")..replaceAll("]", ""));
    Size size = MediaQuery.of(context).size;
    return Container(
            color:Color(0xffE5E5E5),
            child:StaggeredGridView.count(
            crossAxisCount: 4,
            crossAxisSpacing: 8.0,
            mainAxisSpacing: 8.0,
          children: <Widget>[
            Scaffold(
              appBar: AppBar(
              automaticallyImplyLeading: true,
              centerTitle: true,
              title: Text('Examenes'),
              leading: IconButton(icon:Icon(Icons.arrow_back_ios),
                onPressed:() => Navigator.pop(context, false),
              )
            ),
            body:             
            Material(
            color: Colors.white,
            elevation: 2.0,
            borderRadius: BorderRadius.circular(24.0),
            child: Center(
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                              Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(1.0),
                              child: Text("Examen de flexión",textAlign: TextAlign.center, style: TextStyle(
                                fontSize: size.width/10,
                                color: Colors.blueAccent,
                              ),),
                            ),
                            Padding(
                              padding: EdgeInsets.all(1.0),
                              child: Text("Tu objetivo es llegar a: 180°", style: TextStyle(
                                fontSize: size.width/20,
                              ),),
                            ),
                            Padding(
                              padding: EdgeInsets.all(1.0),
                              child: CircularPercentIndicator(
                          radius: 120.0,
                          lineWidth: 13.0,
                          animation: true,
                          percent: angulo/180,
                          center: new Text(
                            angulo.toString()+"°",
                            style:
                                new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
                          ),
                          footer: new Text(
                            "Inclinometro",
                            style:
                                new TextStyle(fontWeight: FontWeight.bold, fontSize: size.width/20),
                          ),
                          circularStrokeCap: CircularStrokeCap.round,
                          progressColor: Colors.blue,
                        ),
                            ),

                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),),
              RaisedButton(
                onPressed: getMeasure,
                child: const Text(
                  'Tomar medida',
                  style: TextStyle(fontSize: 18)
                ),
                textColor: Colors.white,
              ),
              RaisedButton(
                onPressed: () async {
                  subirMedida(angulo);
                },
                child: const Text(
                  'Subir medida',
                  style: TextStyle(fontSize: 18)
                ),
                textColor: Colors.white,
              ),
          ],
          staggeredTiles: [
            StaggeredTile.extent(4, size.height/2),
            StaggeredTile.extent(2, size.height/12),
            StaggeredTile.extent(2, size.height/12),
          ],
        ));
  }
}