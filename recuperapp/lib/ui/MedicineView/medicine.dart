import 'package:flutter/material.dart';
import 'package:recuperapp/blocs/blocs.dart';

import 'components/homepage.dart';

class Medicine extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return Container(child:BlocProvider(
      bloc: MedicinesBloc(),
      child: BlocProvider(bloc:NewEntryBloc(),child:HomePage())
      ));
  }
}
