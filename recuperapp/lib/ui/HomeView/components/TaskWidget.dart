import 'package:flutter/material.dart';
import 'package:recuperapp/ui/ExercisesView/exercise.dart';

import '../../../assets/my_custom_icons_icons.dart';
import '../../../models/models.dart';
import '../../../blocs/blocs.dart';

typedef void OnAccept(Task task);

class TasksWidget extends StatelessWidget {
  TasksWidget({@required this.onAccept});
  final OnAccept onAccept;

  @override
  Widget build(BuildContext context) {
    TaskBloc tareasBloc = BlocProvider.of<TaskBloc>(context);
    return StreamBuilder(
        stream: tareasBloc.tasks,
        builder: (BuildContext context, AsyncSnapshot<List<Task>> snapshot) =>
            (snapshot.hasData)
                ? ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (BuildContext context, int index) =>
                        TaskWidget(
                          task: snapshot.data[index],
                          onAccept: onAccept,
                        ))
                : Center(child: CircularProgressIndicator()));
  }
}

class TaskWidget extends StatelessWidget {
  TaskWidget({@required this.task, this.onAccept})
      : super(key: Key(task.toString()));
  final Task task;
  final OnAccept onAccept;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
        child: Container(
            margin: EdgeInsets.symmetric(
                horizontal: (1 / 40) * size.width,
                vertical: (1 / 80) * size.height),
            child: Row(
              children: <Widget>[
                Container(
                    width: (1 / 8) * size.width,
                    height: (1 / 8) * size.width,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                            Radius.circular((1 / 2 * 8) * size.width))),
                    child: Icon(
                      _getIcon(),
                      size: (2 / 3) * (1 / 8) * size.width,
                      color: _getColor(context),
                    )),
                Expanded(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                      Text(task.name, style: TextStyle(fontSize: 18)),
                      Text(task.category.toString().split('.')[1],
                          style:
                              TextStyle(color: Colors.grey[400], fontSize: 16,fontStyle: FontStyle.italic))
                    ])),
                Padding(
                    child: _durationOrAlarm(),
                    padding: EdgeInsets.only(left: (1 / 60) * size.width)),
              ],
            )),
        onTap: ()
        
         async {
            print('mmmmmm');
            print('mmmmmm');
            print('mmmmmm');
            print('mmmmmm');
          switch (task.category) {
            case Category.Exam:
              break;
            case Category.Exercise:
            print('mmmmmm');
            print('mmmmmm');
            print('mmmmmm');
            print('mmmmmm');

              final result = await Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          ExerciseWidget(idExercise: task.id)));
              if (result != null && result) {
                onAccept(task);
              }
              break;
            case Category.Medicine:
              break;
              break;
            case Category.Medicine:
              break;
          }
        });
  }

  Widget _durationOrAlarm() {
    if (task.time != null)
      return Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
        Text(
          '${task.time} min ',
          style: TextStyle(color: Colors.grey, fontSize: 16),
        ),
        Icon(Icons.access_time, color: Colors.grey)
      ]);
    else
      return Icon(Icons.access_alarm, color: Colors.grey);
  }

  IconData _getIcon() {
    switch (task.category) {
      case Category.Exercise:
        return Icons.directions_run;
      case Category.Exam:
        return Icons.event_note;
      case Category.Medicine:
        return MyCustomIcons.Pildora;
      default:
        return Icons.error;
    }
  }

  Color _getColor(BuildContext context) {
    return Theme.of(context).primaryColor;
  }
}
