import 'package:flutter/material.dart';

/// esta clase widget pinta la diagonal del fondo
class HomeBackground extends StatelessWidget {
  HomeBackground({this.color: Colors.black});
  final Color color;

  @override
  Widget build(BuildContext context) => ClipPath(
        child: Container(color: color),
        clipper: Clipper(),
      );
}

// en el clipper se definen los pasos para pintar la diagonal
class Clipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.moveTo(0, 0);
    path.lineTo(0, (1 / 4) * size.height);
    path.arcToPoint(Offset((2 / 3) * size.width, 0),
        radius: Radius.circular((1.5)*size.width));
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
