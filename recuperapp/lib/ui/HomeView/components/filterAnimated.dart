import 'dart:math' as math;
import 'package:flutter/material.dart';

import '../../../assets/my_custom_icons_icons.dart';
import '../../../models/models.dart';
import '../../../blocs/blocs.dart';

// el filtro animado de las tareas pendientes
enum Filter { None, Medicines, Exercises, Exams }
typedef void OnClick(Filter item);

class FilterAnimated extends StatefulWidget {
  const FilterAnimated({Key key, this.icons}) : super(key: key);
  final List<Icon> icons;

  @override
  _FilterAnimatedState createState() => _FilterAnimatedState();
}

class _FilterAnimatedState extends State<FilterAnimated>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation<Color> _colorAnimation;
  Filter currentFilter = Filter.None;
  final double expandedSize = 180.0;
  final double hiddenSize = 20.0;
  TaskBloc _tareasBloc;

  @override
  void initState() {
    super.initState();
   
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200));
    _colorAnimation = ColorTween(
            begin: const Color(0XFF023555),
            end: const Color(0XFF022545))
        .animate(_animationController);
     _tareasBloc = BlocProvider.of<TaskBloc>(context);
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: expandedSize,
      height: expandedSize,
      child: AnimatedBuilder(
        animation: _animationController,
        builder: (BuildContext context, Widget child) {
          return Transform.translate(
              offset: Offset((expandedSize / 2) - 30, 0),
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  _buildExpandedBackground(),
                  _buildOption(Icons.check_circle, 0.0, Filter.None),
                  _buildOption(
                      MyCustomIcons.Pildora, -math.pi / 3, Filter.Medicines),
                  _buildOption(
                      Icons.directions_run, -2 * math.pi / 3, Filter.Exercises),
                  _buildOption(Icons.event_note, math.pi, Filter.Exams),
                  _buildFabCore(),
                ],
              ));
        },
      ),
    );
  }

  Widget _buildOption(IconData icon, double angle, Filter filter) {
    if (_animationController.isDismissed) {
      return Container();
    }
    double iconSize = 0.0;
    if (_animationController.value > 0.8) {
      iconSize = 26.0 * (_animationController.value - 0.8) * 5;
    }
    return Transform.rotate(
      angle: angle,
      child: Align(
        alignment: Alignment.topCenter,
        child: Padding(
          padding: EdgeInsets.only(top: 8.0),
          child: IconButton(
            onPressed: () {
              _onIconClick(context, filter);
            },
            icon: Transform.rotate(
              angle: -angle,
              child: Icon(
                icon,
                color: Colors.white,
              ),
            ),
            iconSize: iconSize,
            alignment: Alignment.center,
            padding: EdgeInsets.all(0.0),
          ),
        ),
      ),
    );
  }

  Widget _buildExpandedBackground() {
    double size =
        hiddenSize + (expandedSize - hiddenSize) * _animationController.value;
    return Container(
      height: size,
      width: size,
      decoration: BoxDecoration(
          shape: BoxShape.circle, color: Theme.of(context).primaryColor),
    );
  }

  Widget _buildFabCore() {
    double scaleFactor = 2 * (_animationController.value - 0.5).abs();
    return FloatingActionButton(
      onPressed: _onFabTap,
      child: Transform(
        alignment: Alignment.center,
        transform: Matrix4.identity()..scale(1.0, scaleFactor),
        child: Icon(
          _animationController.value > 0.5 ? Icons.close : Icons.filter_list,
          color: Colors.white,
          size: 26.0,
        ),
      ),
      backgroundColor: _colorAnimation.value,
    );
  }

  open() {
    if (_animationController.isDismissed) {
      _animationController.forward();
    }
  }

  close() {
    if (_animationController.isCompleted) {
      _animationController.reverse();
    }
  }

  _onFabTap() {
    if (_animationController.isDismissed) {
      open();
    } else {
      close();
    }
  }

  _onIconClick(BuildContext context, Filter filter) {
    _tareasBloc.filter(_ofFilter(filter));
    close();
  }

  Category _ofFilter(Filter filter) {
    switch (filter) {
      case Filter.Medicines:
        return Category.Medicine;
      case Filter.Exercises:
        return Category.Exercise;
      case Filter.Exams:
        return Category.Exam;
      default:
        return null;
    }
  }
}
