import 'package:flutter/material.dart';
import '../../../models/models.dart';

// este componente muestra la imagen y el nombre del usuario
class ProfileWidget extends StatelessWidget {
  ProfileWidget(
      {@required this.user,
      this.color: Colors.deepOrange,
      this.iconColor: Colors.white,
      @required this.onImageClick});
  final User user;
  final Color color;
  final Color iconColor;
  // cuando se preciona la imagen
  final VoidCallback onImageClick;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Row(mainAxisAlignment: MainAxisAlignment.center, children: [
      Flexible(child: _image(size)),
      Flexible(
          child: Container(
              padding: EdgeInsets.only(left: (1 / 20) * size.width),
              child: Text(
                _getName(),
                style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold),
              )))
    ]);
  }

  Widget _image(Size size) {
    return GestureDetector(
        onTap: onImageClick,
        child: Container(
            width: (1 / 7) * size.height,
            height: (1 / 7) * size.height,
            decoration: BoxDecoration(
                color: color,
                image: (user.img != null)
                    ? DecorationImage(
                        image: NetworkImage(user.img), fit: BoxFit.fill)
                    : null,
                borderRadius:
                    BorderRadius.all(Radius.circular((1 / 14) * size.height))),
            child: (user.img == null)
                ? Icon(Icons.person,
                    size: (3 / 4) * (1 / 7) * size.height, color: iconColor)
                : Container()));
  }

  String _getName() => (user.nombre != null) ? user.nombre : 'None';
}
