import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import '../../assets/my_custom_icons_icons.dart';
import '../../models/models.dart';
import '../../blocs/blocs.dart';


import 'components/ProfileWidget.dart';
import 'components/TaskWidget.dart';
import 'components/filterAnimated.dart';
import 'components/HomeBackground.dart';

final List<String> months = [
  'Enero',
  'Febrero',
  'Marzo',
  'Abril',
  'Mayo',
  'Junio',
  'Julio',
  'Agosto',
  'Septiembre',
  'Octubre',
  'Noviembre',
  'Diciembre'
];

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  bool hasConection;
  Observable<User> _userStream;
  TaskBloc _tareasBloc;

  @override
  initState() {
    super.initState();
    hasConection = ConnectionStatus.getInstance().hasConnection;
    ConnectionStatus.getInstance().connectionChange.listen((bool con) {
      setState(() {
        hasConection = con;
      });
    });

    _userStream = BlocProvider.of<UserBloc>(context).infoUsuario;
    _tareasBloc = BlocProvider.of<TaskBloc>(context);
  }

  Future<bool> cerrarSesion() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Vas a cerrar sesión'),
        content: new Text('¿Seguro?'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () {int count = 0; 
            Navigator.of(context).popUntil((_) => count++ >= 2);},
            child: new Text('Si'),
          ),
        ],
      ),
    ) ?? false;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    List<double> margin = [(1 / 30) * size.width, (1 / 30) * size.height];
    return Scaffold(
        drawer: _drawer(size, context),
        appBar: AppBar(
            centerTitle: true,
            title: Text('Inicio'),
            actions: (!hasConection)
                ? [
                    Padding(
                        padding: EdgeInsets.only(right: margin[0]),
                        child: Icon(Icons.signal_wifi_off, color: Colors.orange[800]))
                  ]
                : null),
        body: Stack(
          children: <Widget>[
            HomeBackground(color: Theme.of(context).primaryColorDark),
            Column(children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(vertical: margin[1]),
                child: StreamBuilder(
                    stream: _userStream,
                    builder:
                        (BuildContext contex, AsyncSnapshot<User> snapshot) =>
                            (snapshot.hasData)
                                ? ProfileWidget(
                                    user: snapshot.data,
                                    onImageClick: () {
                                      Navigator.pushNamed(context, '/profile');
                                    })
                                : Center(child: CircularProgressIndicator())),
              ),
              Expanded(
                  child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    _buildMyTasksHeader(context),
                    Expanded(child: TasksWidget(onAccept: (Task task) {
                      print(task.idTask);
                      _tareasBloc.markAsDone(task.idTask);
                    }))
                  ],
                ),
              ))
            ]),
            Container(
                margin: EdgeInsets.only(top: (1 / 12) * size.height),
                alignment: Alignment.topRight,
                child: FilterAnimated())
          ],
        ));
  }

  Widget _drawer(Size size, BuildContext context) {
    return SizedBox(
        width: (2 / 3) * size.width,
        child: Drawer(
            child: ListView(padding: EdgeInsets.zero, children: <Widget>[
          DrawerHeader(
            child: Container(),
            decoration: BoxDecoration(color: Theme.of(context).primaryColor),
          ),
          Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: (1 / 40) * size.width,
                  vertical: (1 / 60) * size.height),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      Icon(MyCustomIcons.Pildora),
                      FlatButton(
                          child: Text('Medicamentos',
                              style: TextStyle(
                                  fontSize: 18, fontStyle: FontStyle.italic)),
                          onPressed: () {
                            Navigator.popAndPushNamed(context, '/medicine');
                          })
                    ]),
                    Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      Icon(Icons.directions_run),
                      FlatButton(
                          child: Text('Ejercicios',
                              style: TextStyle(
                                  fontSize: 18, fontStyle: FontStyle.italic)),
                          onPressed: () {
                            Navigator.popAndPushNamed(context, '/exercises');
                          })
                    ]),
                    Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      Icon(Icons.event_note),
                      FlatButton(
                          child: Text('Exámenes',
                              style: TextStyle(
                                  fontSize: 18, fontStyle: FontStyle.italic)),
                          onPressed: () {
                            Navigator.popAndPushNamed(context, '/exams');
                          })
                    ]),
                    Divider(),                    
                    Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      Icon(Icons.exit_to_app, color: Colors.red),
                      FlatButton(
                          child: Text('Salir',
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.red,
                                  fontStyle: FontStyle.italic)),
                          onPressed: cerrarSesion)
                    ]),
                  ]))
        ])));
  }

  Widget _buildMyTasksHeader(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    DateTime date = DateTime.now();
    return Container(
        padding: EdgeInsets.only(
            left: (1 / 12) * size.width, top: (1 / 30) * size.height),
        child: Row(children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Tareas Pendientes',
                style: TextStyle(fontSize: 26.0),
              ),
              Text(
                '${months[date.month - 1]} ${date.day}, ${date.year}',
                style: TextStyle(color: Colors.grey, fontSize: 12.0),
              )
            ],
          ),
        ]));
  }
}
