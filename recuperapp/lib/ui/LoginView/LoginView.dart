import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rxdart/rxdart.dart';
import 'package:toast/toast.dart';

import '../../blocs/blocs.dart';
import '../navbar.dart';

class LoginView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LongViewState();
}

class _LongViewState extends State<LoginView> {
  bool hasConection = false;

  final _usernameFieldController = TextEditingController();
  final _passwordFieldController = TextEditingController();
  Observable<String> _passWordError;
  Observable<String> _emailError;
  Observable<String> _stream;
  Function _logginFunction;

  @override
  initState() {
    ConnectionStatus.getInstance().connectionChange.listen((bool con) {
      setState(() {
        hasConection = con;
      });
    });

    super.initState();
    var bloc = BlocProvider.of<AuthBloc>(context);
    _stream = bloc.answer;
    _emailError = bloc.emailError;
    _passWordError = bloc.passWordError;
    _logginFunction = bloc.loggin;
    _stream.listen((String idUsuario) => Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => BlocProvider(
                bloc: ExerciseBlock(),
                child: BlocProvider(
                    bloc: TaskBloc(idUsuario),
                    child: BlocProvider(
                        bloc: UserBloc(idUsuario), child: PrincipalView()))))));
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    List<double> margin = [(1 / 30) * size.width, (1 / 30) * size.height];
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
            actions: (!hasConection)
                ? [
                    Padding(
                        padding: EdgeInsets.only(right: margin[0]),
                        child: Icon(Icons.signal_wifi_off, color: Colors.orange[800]))
                  ]
                : null),
        body: Stack(children: [
          Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 2 * margin[0], vertical: margin[1]),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(children: <Widget>[
                      Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: (1 / 3) * margin[1]),
                          child: StreamBuilder<String>(
                              initialData: null,
                              stream: _emailError,
                              builder: (BuildContext context,
                                      AsyncSnapshot<String> snapshot) =>
                                  TextField(
                                      controller: _usernameFieldController,
                                      inputFormatters: [
                                        LengthLimitingTextInputFormatter(30)
                                      ],
                                      keyboardType: TextInputType.emailAddress,
                                      decoration: InputDecoration(
                                          hintText: 'Correo',
                                          labelText: 'Correo',
                                          errorText: snapshot.data,
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      margin[1])))))),
                      Container(
                          margin: EdgeInsets.symmetric(
                              vertical: (1 / 3) * margin[1]),
                          child: StreamBuilder<String>(
                              initialData: null,
                              stream: _passWordError,
                              builder: (BuildContext context,
                                      AsyncSnapshot<String> snapshot) =>
                                  TextField(
                                      obscureText: true,
                                      inputFormatters: [
                                        LengthLimitingTextInputFormatter(20)
                                      ],
                                      controller: _passwordFieldController,
                                      decoration: InputDecoration(
                                          hintText: 'Contraseña',
                                          labelText: 'Contraseña',
                                          errorText: snapshot.data,
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      margin[1])))))),
                      FlatButton(
                          child: Container(
                              padding:
                                  EdgeInsets.symmetric(vertical: margin[1]),
                              alignment: Alignment.center,
                              child: Text('Ingresar',
                                  style: TextStyle(color: Colors.white),
                                  textScaleFactor: 1.2)),
                          color: Theme.of(context).buttonColor,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0)),
                          onPressed: () {
                            if (hasConection){
                              _logginFunction(UserAut(
                                  email: _usernameFieldController.text,
                                  pass: _passwordFieldController.text));
                              Toast.show("Estamos cargando tus datos...", context, duration: 3, gravity:  Toast.CENTER);    
                            }
                            else
                                Toast.show("Necesitas conectarte a internet para poder ingresar.", context, duration: 4, gravity:  Toast.CENTER);
                                 
                          }),
                      FlatButton(
                        onPressed: () {
                            Toast.show("Estamos trabajando esta funcionalidad :D", context, duration: 3, gravity:  Toast.CENTER);
                        },
                        child: Text('Olvidaste la contraseña?',
                            style: TextStyle(
                                color: Theme.of(context).accentColor,
                                fontWeight: FontWeight.bold)),
                      )
                    ]),
                    Container(
                        child: Column(children: <Widget>[
                      Text("¿No tienes una cuenta?",
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      FlatButton(
                          onPressed: () {
                            Toast.show("Estamos trabajando esta funcionalidad :D", context, duration: 3, gravity:  Toast.CENTER);
                          },
                          child: Text('Crear cuenta',
                              style: TextStyle(
                                  color: Theme.of(context).accentColor,
                                  fontWeight: FontWeight.bold)))
                    ]))
                  ]))
        ]));
  }
}
