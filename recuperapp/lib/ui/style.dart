import 'package:flutter/material.dart'; 

const Color primaryColor = const Color(0XFF023555);
const Color primaryColorDark = const Color(0XFF022545);
const Color primaryColorComplement = Colors.white;
const Color highlight = Colors.blueAccent;