import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:recuperapp/ui/ProfileView/ProfilePage.dart';
import 'package:recuperapp/ui/statisticsView/photoGallery.dart';

import 'MedicineView/medicine.dart';
import 'statisticsView/stats.dart';
import 'ExamsView/exams.dart';
import 'ExercisesView/exercises.dart';

import 'chatView/chat.dart';
import 'HomeView/home.dart';

class PrincipalView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return HomePage(<Destination>[
      Destination(0, 'Salud', Icons.favorite,
          {'/': (BuildContext context) => StatsView(),
          "/gallery": (BuildContext context) => GalleryPage(),}),
      Destination(1, 'Inicio', Icons.home, {
        '/': (BuildContext context) => HomeView(),
        '/exercises': (BuildContext context) => ExerciesView(),
        '/exams': (BuildContext context) => ExamsView(),
        '/medicine': (BuildContext context) => Medicine(),
        '/profile' : (BuildContext context) => ProfilePage(),
      }),
      Destination(2, 'Hey Doc!', Icons.chat_bubble,
          {'/': (BuildContext context) => ChatView()}),
    ],initIndex: 1);
  }
}

class NavigationWrapper extends StatefulWidget {
  const NavigationWrapper({Key key, this.routes}) : super(key: key);
  final Map<String, Function> routes;
  @override
  _NavigationWrapperState createState() => _NavigationWrapperState();
}

class _NavigationWrapperState extends State<NavigationWrapper> {
  @override
  Widget build(BuildContext context) {
    return Navigator(
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          settings: settings,
          builder: (BuildContext context) {
            return widget.routes[settings.name](context);
          },
        );
      },
    );
  }
}

class Destination {
  const Destination(this.index, this.title, this.icon, this.routes);
  final int index;
  final String title;
  final IconData icon;
  final Map<String, Function> routes;
}

class HomePage extends StatefulWidget {
  const HomePage(this.allDestinations, {this.initIndex});
  final List<Destination> allDestinations;
  final int initIndex;
  @override
  _HomePageState createState() => _HomePageState(initIndex: initIndex);
}

class _HomePageState extends State<HomePage>
    with TickerProviderStateMixin<HomePage> {
  List<AnimationController> _faders;
  List<Key> _destinationKeys;
  int _currentIndex;

  _HomePageState({int initIndex}) {
    _currentIndex = (initIndex != null) ? initIndex : 0;
  }

  @override
  void initState() {
    _destinationKeys = List<Key>.generate(
        widget.allDestinations.length, (int index) => GlobalKey()).toList();
    super.initState();
    _faders = widget.allDestinations
        .map<AnimationController>((Destination destination) {
      return AnimationController(
          vsync: this, duration: Duration(milliseconds: 200));
    }).toList();
    _faders[_currentIndex].value = 1.0;
  }

  @override
  void dispose() {
    for (AnimationController controller in _faders) controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return new Future.value(false);
      },
          child: Scaffold(
        body: SafeArea(
          top: false,
          child: Stack(
            fit: StackFit.expand,
            children: widget.allDestinations.map((Destination destination) {
              final Widget view = FadeTransition(
                opacity: _faders[destination.index]
                    .drive(CurveTween(curve: Curves.fastOutSlowIn)),
                child: KeyedSubtree(
                    key: _destinationKeys[destination.index],
                    child: NavigationWrapper(routes: destination.routes)),
              );
              if (destination.index == _currentIndex) {
                _faders[destination.index].forward();
                return view;
              } else {
                _faders[destination.index].reverse();
                if (_faders[destination.index].isAnimating) {
                  return IgnorePointer(child: view);
                }
                return Offstage(child: view);
              }
            }).toList(),
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          unselectedItemColor: Colors.blueGrey[200],
          selectedItemColor: Theme.of(context).primaryColor,
          currentIndex: _currentIndex,
          onTap: (int index) {
            setState(() {
              _currentIndex = index;
            });
          },
          items: widget.allDestinations.map((Destination destination) {
            return BottomNavigationBarItem(
                icon: Icon(destination.icon), title: Text(destination.title));
          }).toList(),
        ),
      ),
    );
  }
}
