import 'package:flutter/material.dart';
import 'package:recuperapp/blocs/blocs.dart';
import '../../models/models.dart';
import '../../resources/providers.dart';
import '../style.dart';

class ExerciseWidget extends StatefulWidget {
  final String idExercise;
  ExerciseWidget({@required this.idExercise});

  @override
  State<StatefulWidget> createState() =>
      _ExerciseWidgetState(idExercise: idExercise);
}

class _ExerciseWidgetState extends State<ExerciseWidget> {
  _ExerciseWidgetState({@required this.idExercise});
  final String idExercise;
  bool hasConection;

  @override
  initState() {
    super.initState();
    hasConection = ConnectionStatus.getInstance().hasConnection;
    ConnectionStatus.getInstance().connectionChange.listen((bool con) {
      setState(() {
        hasConection = con;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    List<double> margin = [(1 / 30) * size.width, (1 / 30) * size.height];
    return FutureBuilder(
        future: ExerciseProvider().getExercise(idExercise),
        builder: (BuildContext context, AsyncSnapshot<Exercise> snapshot) {
          if (snapshot.hasData) {
            Exercise exercise = snapshot.data;
            Size size = MediaQuery.of(context).size;
            return Scaffold(
                appBar: AppBar(
                    backgroundColor: primaryColor,
                    actions: (!hasConection)
                        ? [
                            Padding(
                                padding: EdgeInsets.only(right: margin[0]),
                                child: Icon(Icons.signal_wifi_off, color: Colors.orange[800]))
                          ]
                        : null,
                    centerTitle: true,
                    title: Text(exercise.name, textAlign: TextAlign.center)),
                body: Column(children: [
                  Padding(
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              'Duracion: ${exercise.duracion} min ',
                              style:
                                  TextStyle(color: Colors.grey, fontSize: 18),
                            ),
                            Icon(Icons.access_alarms, color: Colors.grey)
                          ]),
                      padding: EdgeInsets.symmetric(
                          vertical: (1 / 60) * size.height)),
                  Container(
                      margin: EdgeInsets.symmetric(
                          vertical: (1 / 50) * size.height),
                      child: Text(exercise.name,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold))),
                  Expanded(
                      child: Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: (1 / 60) * size.height,
                              horizontal: (1 / 30) * size.width),
                          child: ListView(children: <Widget>[
                            Text(
                              exercise.descripcion,
                              textAlign: TextAlign.center,
                            ),
                            Container(
                                padding: EdgeInsets.only(
                                    top: (1 / 60) * size.height),
                                alignment: Alignment.centerRight,
                                child: Text(
                                    'Repeticiones: ${exercise.repetitions}',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey))),
                            Image.network(exercise.imgSource),
                            FlatButton(
                                child: Container(
                                    padding: EdgeInsets.symmetric(vertical: 20),
                                    alignment: Alignment.center,
                                    child: Text(
                                      'Completado',
                                    )),
                                color: primaryColor,
                                textColor: primaryColorComplement,
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(10.0)),
                                onPressed: () {
                                    Navigator.of(context).pop(true);
                                }),
                          ])))
                ]));
          }
          return Center(child: CircularProgressIndicator());
        });
  }
}
