import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';

import '../../models/models.dart';
import '../../blocs/blocs.dart';

class ExerciesView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ExerciesViewState();
}

class _ExerciesViewState extends State<ExerciesView> {
  bool hasConection;

  @override
  initState() {
    super.initState();
    hasConection = ConnectionStatus.getInstance().hasConnection;
    ConnectionStatus.getInstance().connectionChange.listen((bool con) {
      setState(() {
        hasConection = con;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil.getInstance()..init(context);
    Size size = MediaQuery.of(context).size;
    List<double> margin = [(1 / 30) * size.width, (1 / 30) * size.height];
    return Scaffold(
        appBar: AppBar(
            centerTitle: true,
            title: Text('Ejercicios'),
            actions: (!hasConection)
                ? [
                    Padding(
                        padding: EdgeInsets.only(right: margin[0]),
                        child: Icon(Icons.signal_wifi_off, color: Colors.orange[800]))
                  ]
                : null),
        body: ExercisesList());
  }
}

class ExercisesList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ExerciseBlock _exerciseBlock = BlocProvider.of<ExerciseBlock>(context);
    return Column(mainAxisSize: MainAxisSize.max, children: [
      StreamBuilder(
          stream: _exerciseBlock.muculos,
          builder:
              (BuildContext context, AsyncSnapshot<List<String>> snapshot) =>
                  (snapshot.hasData)
                      ? DropdownButton<String>(
                          hint: Text('Buscar por músculo'),
                          items: snapshot.data
                              .map((String value) => DropdownMenuItem<String>(
                                    value: value,
                                    child: new Text(value),
                                  ))
                              .toList(),
                          onChanged: (String changedValue) {
                            _exerciseBlock.filter(changedValue);
                          },
                        )
                      : Center(child: CircularProgressIndicator())),
      StreamBuilder(
          stream: _exerciseBlock.ejercicios,
          builder:
              (BuildContext context, AsyncSnapshot<List<Exercise>> snapshot) =>
                  (snapshot.hasData)
                      ? Expanded(
                          child: ListView.builder(
                              itemCount: snapshot.data.length,
                              itemBuilder: (BuildContext ctxt, int index) {
                                Exercise exercise = snapshot.data[index];
                                return ExcerciseView(exercise: exercise);
                              }))
                      : Center(child: CircularProgressIndicator()))
    ]);
  }
}

typedef void OnClick(Exercise exercise);

class ExcerciseView extends StatefulWidget {
  ExcerciseView({@required this.exercise});
  final Exercise exercise;

  @override
  State<StatefulWidget> createState() =>
      _ExcerciseViewState(exercise: exercise);
}

class _ExcerciseViewState extends State<ExcerciseView> {
  _ExcerciseViewState({@required this.exercise});
  final Exercise exercise;

  bool hasConection;

  @override
  initState() {
    super.initState();
    hasConection = ConnectionStatus.getInstance().hasConnection;
    ConnectionStatus.getInstance().connectionChange.listen((bool con) {
      setState(() {
        hasConection = con;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    TaskBloc tareasBloc = BlocProvider.of<TaskBloc>(context);
    return Column(mainAxisSize: MainAxisSize.min, children: [
      ExpansionTile(
        leading: Container(
          decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              borderRadius: BorderRadius.circular(10)),
          padding: EdgeInsets.all(5),
          child: Icon(Icons.directions_run, color: Colors.white, size: 25),
        ),
        key: PageStorageKey<Exercise>(exercise),
        title: Center(child: Text(exercise.name, textAlign: TextAlign.center)),
        children: <Widget>[
          Container(
              color: Colors.grey[200],
              child: Container(
                  margin: EdgeInsets.only(right: 15, left: 15, top: 7),
                  child: Column(children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          'Duracion: ${exercise.duracion} min ',
                          style: TextStyle(color: Colors.grey, fontSize: 16),
                        ),
                        Icon(Icons.access_alarms, color: Colors.grey)
                      ],
                    ),
                    Text(exercise.name,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold)),
                    Divider(),
                    Text(
                      exercise.descripcion,
                      textAlign: TextAlign.center,
                    ),
                    Align(
                        alignment: Alignment.centerRight,
                        child: FlatButton(
                          onPressed: () {
                              tareasBloc.addExercise(exercise);
                              Navigator.of(context).pop();
                          },
                          child: Text('Agregar',
                              style: TextStyle(
                                  color: Theme.of(context).primaryColor,
                                  fontSize: 20)),
                        ))
                  ])))
        ],
      ),
    ]);
  }
}
