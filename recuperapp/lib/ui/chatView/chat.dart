import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:recuperapp/blocs/ChatBloc.dart';
import 'package:recuperapp/blocs/blocs.dart';
import 'package:recuperapp/models/models.dart';
import 'dart:io';
import 'package:toast/toast.dart';

class ChatView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ChatViewState();
}

class _ChatViewState extends State<ChatView> {
  bool hasConection;

  @override
  initState() {
    super.initState();
    hasConection = ConnectionStatus.getInstance().hasConnection;
    ConnectionStatus.getInstance().connectionChange.listen((bool con) {
      setState(() {
        hasConection = con;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    UserBloc userBloc = BlocProvider.of<UserBloc>(context);
    Size size = MediaQuery.of(context).size;
    List<double> margin = [(1 / 30) * size.width, (1 / 30) * size.height];
    return Scaffold(
        appBar: AppBar(
            centerTitle: true,
            title: Text('Chat'),
            actions: (!hasConection)
                ? [
                    Padding(
                        padding: EdgeInsets.only(right: margin[0]),
                        child: Icon(Icons.signal_wifi_off, color: Colors.orange[800]))
                  ]
                : null),
        body: StreamBuilder(
            stream: userBloc.infoUsuario,
            builder: (BuildContext context, AsyncSnapshot<User> snap) =>
                (snap.hasData)
                    ? BlocProvider(
                        bloc: ChatBloc(snap.data, snap.data.chat),
                        child: ChatScreen(snap.data))
                    : Center(child: CircularProgressIndicator())));
  }
}

@override
class ChatMessage extends StatelessWidget {
  ChatMessage({this.msg, this.align});
  final Mensage msg;
  final bool align;

  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Flexible(
          child: Container(
              margin:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10),
              alignment: (align) ? Alignment.centerRight : null,
              child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.blueGrey[200]),
                  child: Column(
                    crossAxisAlignment: (align)
                        ? CrossAxisAlignment.end
                        : CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: const EdgeInsets.all(4.0),
                        child: Text(
                          msg.msg,
                          textScaleFactor: 1.1,
                        ),
                      ),
                      Container(
                          margin: const EdgeInsets.all(4.0),
                          child: Text(
                            '${msg.time.hour}:${msg.time.minute}   ${msg.time.day}/${msg.time.month}/${msg.time.year}',
                            style: TextStyle(color: Colors.grey[600]),
                          ))
                    ],
                  ))))
    ]);
  }
}

class ChatScreen extends StatelessWidget {
  final TextEditingController _textController = TextEditingController();
  ChatScreen(this.user);
  final User user;

  @override
  Widget build(BuildContext context) {
    ChatBloc chatBloc = BlocProvider.of<ChatBloc>(context);
    return Column(children: <Widget>[
      StreamBuilder(
          stream: chatBloc.mensajes,
          builder: (BuildContext contex, AsyncSnapshot<List<Mensage>> snap) {
            return (snap.hasData)
                ? Flexible(
                    child: ListView.builder(
                    padding: EdgeInsets.all(8.0),
                    reverse: true,
                    itemBuilder: (_, int index) => ChatMessage(
                        msg: snap.data[index],
                        align: user.id == snap.data[index].user.id),
                    itemCount: snap.data.length,
                  ))
                : Center(child: CircularProgressIndicator());
          }),
      Divider(height: 1.0),
      Container(
          decoration: BoxDecoration(color: Colors.grey[200]),
          child: IconTheme(
              data: IconThemeData(color: Theme.of(context).primaryColor),
              child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Row(children: <Widget>[
                    Flexible(
                      child: TextField(
                        controller: _textController,
                        onSubmitted: (String text) {
                          if (text.isNotEmpty)
                            chatBloc.send(Mensage(
                                msg: text, time: DateTime.now(), user: user));
                        },
                        decoration: InputDecoration.collapsed(
                            hintText: "Envía un mensaje"),
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.symmetric(horizontal: 4.0),
                        child: IconButton(
                            icon: Icon(Icons.send,
                                color: Theme.of(context).primaryColor),
                            onPressed: () async {
                              if (_textController
                                  .text.isNotEmpty) if (_textController
                                      .text.length >
                                  50) {
                                Toast.show(
                                    "Envía tu mensaje con menos de 50 caracteres",
                                    context,
                                    duration: 3,
                                    gravity: Toast.CENTER);
                              } else {
                                chatBloc.send(Mensage(
                                    msg: _textController.text,
                                    time: DateTime.now(),
                                    user: user));
                                _textController.clear();

                                try {
                                  final result = await InternetAddress.lookup(
                                      'google.com');
                                  if (result.isNotEmpty &&
                                      result[0].rawAddress.isNotEmpty) {
                                    print('connected');
                                  }
                                } on SocketException catch (_) {
                                  Toast.show(
                                      "No hay internet. Cuando te conectes tu mensaje será enviado",
                                      context,
                                      duration: 12,
                                      gravity: Toast.CENTER);
                                }
                              }
                            })),
                  ]),
                  decoration: BoxDecoration(
                      border:
                          Border(top: BorderSide(color: Colors.grey[200])))))),
    ]);
  }
}
