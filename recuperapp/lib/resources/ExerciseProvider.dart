import 'package:cloud_firestore/cloud_firestore.dart';
import '../models/models.dart';

///
/// Manejador de ejercicios en firebase
///
class ExerciseProvider {
  /// Este es el objeto del singleton
  static final ExerciseProvider _single = ExerciseProvider._internal();

  /// Referencia a la Colecion
  final CollectionReference _exercises =
      Firestore.instance.collection('ejercicios');

  /// Constructor publico
  factory ExerciseProvider() => _single;

  /// Constructor privado
  ExerciseProvider._internal();

  /// Retorna todos los musculos
  Future<List<String>> getMusculos() async {
    Set<String> ans = Set();
    (await getExercises()).forEach((Exercise e) => ans.addAll(e.muscles));
    return ans.toList();
  }

  /// Retorna el ejercicios con el id dado
  Future<Exercise> getExercise(String id) async {
    DocumentSnapshot doc = await _exercises.document(id).get();
    return Exercise.fromJSON(doc.documentID, doc.data);
  }

  /// retorrna todos los ejercicios
  Future<List<Exercise>> getExercises() async {
    return (await _exercises.getDocuments())
        .documents
        .map((json) => Exercise.fromJSON(json.documentID, json.data))
        .toList();
  }

  /// Retorna todos los ejercicios para un musculo dado
  Future<List<Exercise>> getFilteredExercises({String musculo}) async {
    if (musculo == null)
      return await getExercises();
    else {
      List<DocumentSnapshot> data = (await _exercises
              .where('musculos', arrayContains: musculo)
              .getDocuments())
          .documents;
      var ans = data
          .map((json) => Exercise.fromJSON(json.documentID, json.data))
          .toList();
      return ans;
    }
  }
}
