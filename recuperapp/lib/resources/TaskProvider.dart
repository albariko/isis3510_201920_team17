import 'package:cloud_firestore/cloud_firestore.dart';
import '../models/models.dart';
import 'ExerciseProvider.dart';

///
/// Manejador de las tareas de un usuario en firebase
///
class TaskProvider {
  /// Este es el objeto del singleton
  static TaskProvider _single;

  /// Referencia al documento con los datos
  DocumentReference _tasksDoc;

  /// provedor de todos los ejercicios
  final ExerciseProvider _exerciseprovider = ExerciseProvider();

  /// fecha actual en el formato de firebase
  final String today = _hoy();

  /// Constructor publico
  factory TaskProvider(String userId) {
    if (_single == null) _single = TaskProvider._internal(userId);
    return _single;
  }

  /// Constructor privado
  TaskProvider._internal(String userId) {
    _tasksDoc = Firestore.instance.collection("tareas").document(userId);
  }

  /// Marcar una tarea con id dado como hecha
  markTask(String taskId) async {
    _tasksDoc
        .collection(today)
        .document(taskId)
        .setData({'done': true}, merge: true);
  }

  /// Agregar una tarea entrada por parametro
  addTask(Task e) async {
    _tasksDoc.collection(today).add(e.toJson());
  }

  /// Retorna la ultima fecha con tareas en firebase
  Future<String> _lastTaskDate() async {
    return (await _tasksDoc.get())['lastCollection'];
  }

  /// Retorna las tareas para el dia de hoy
  Future<List<Task>> dailyTask() async {
    String lastCollectionId = await _lastTaskDate();
    if (lastCollectionId == today) {
      return (await _tasksDoc
              .collection(today)
              .where('done', isEqualTo: false)
              .getDocuments())
          .documents
          .map((DocumentSnapshot a) => Task.fromJson(a.documentID, a.data))
          .toList();
    } else {
      int timeL = 10 + await _time(lastCollectionId);
      List<Exercise> ex = await _calculateDailyExercises(timeL);
      List<Task> ans = ex
          .map((Exercise e) => Task(
              name: e.name,
              time: e.duracion,
              id: e.id,
              done: false,
              category: Category.Exercise))
          .toList();
      for (Task task in ans) _tasksDoc.collection(today).add(task.toJson());
      _tasksDoc.setData({'lastCollection': _hoy()});
      return ans;
    }
  }

  ///
  /// Funcion dimanica que retorna el subcnjunto de ejercicios maximo tal
  /// que la suma de sus tiempso sea menor que el parametro
  ///
  Future<List<Exercise>> _calculateDailyExercises(int T) async {
    List<Exercise> e = await _exerciseprovider.getExercises();
    List<List<bool>> m = List(e.length);
    List<List<int>> n = List(e.length);
    m[0] = List(T + 1);
    n[0] = List(T + 1);
    for (int j = 1; j <= T; j++) m[0][j] = (e[0].duracion == j);
    m[0][0] = true;
    for (int i = 1; i < m.length; i++) {
      m[i] = List(T + 1);
      n[i] = List(T + 1);
      for (int j = 0; j <= T; j++) {
        if (m[i - 1][j]) {
          m[i][j] = true;
          n[i][j] = j;
        } else if (j - e[i].duracion >= 0 && m[i - 1][j - e[i].duracion]) {
          m[i][j] = true;
          n[i][j] = j - e[i].duracion;
        } else
          m[i][j] = false;
      }
    }
    int max = 0;
    for (int i = T; i >= 0; i--) {
      if (m[e.length - 1][i]) {
        max = i;
        break;
      }
    }
    List<Exercise> ans = [];
    int i = e.length - 1;
    int j = max;
    while (j != null && j != 0) {
      if (j != n[i][j]) {
        j = n[i][j];
        ans.add(e[i]);
      }
      i--;
    }
    return ans;
  }

  ///  Retorna la suma de los ejercicios de dia dado por parametro
  Future<int> _time(String day) async {
    var tasks = (await _tasksDoc
            .collection(day)
            .getDocuments())
        .documents;
    int count = 0;
    for (DocumentSnapshot a in tasks){
      print(a.data);
      if(a['done']){
       count += a['tiempo'];
      }
    }
    print('$count');
    print('$count');
    print('$count');
    print('$count');
    return count;
  }

  /// retorna la fecha actual en el formato dd-mm-yyyy
  static String _hoy() {
    DateTime now = DateTime.now();
    return '${now.day}-${now.month}-${now.year}';
  }
}
