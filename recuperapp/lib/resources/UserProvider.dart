import 'package:cloud_firestore/cloud_firestore.dart';
import '../models/models.dart';

///
/// Manejador de los datos de usuario en firebase
///
class UserProvider {
  /// Este es el objeto del singleton
  static UserProvider _single;

  /// Referencia al documento con los datos
  DocumentReference _usuarioDoc;

  // Constructor publico
  factory UserProvider(String userId) {
    if (_single == null) _single = UserProvider._internal(userId);
    return _single;
  }

  /// Constructor privado
  UserProvider._internal(String userId) {
    _usuarioDoc = Firestore.instance.collection("usuarios").document(userId);
  }

  /// Retorna los datos del usuario
  Future<User> getUsuario() async {
    DocumentSnapshot document = await _usuarioDoc.get();
    return User.fromJson(document.documentID, document.data);
  }

  /// Cambia los datos del usuario
  /// Si no se pasa un parametro (como null) se mantiene el valor actual
  Future setUsuario(User usuario) async {
    print(usuario.toJson());
    _usuarioDoc.setData(usuario.toJson(), merge: true);
  }
}
