import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:recuperapp/models/models.dart';

///
/// Esta clase brinda soporte al chat
///
class ChatProvider {
  /// vjeto singleton
  static ChatProvider _single;

  /// lista de usuarios del chat
  List<User> _usuarios;

  /// usuario actual
  User current;

  /// indice en la lista de uauarios del usuario actual
  int currentIdPos;

  /// Referencia al documento con los datos
  DocumentReference _chatDoc;

  Future<List<User>> usuarios() async {
    if (_usuarios == null) _usuarios = await _getUsuarios();
    return _usuarios;
  }

  /// Constructor publico
  factory ChatProvider(User current, String chatId) {
    if (_single == null) _single = ChatProvider._internal(current, chatId);
    return _single;
  }

  /// Constructor privado
  ChatProvider._internal(this.current, chatId) {
    _chatDoc = Firestore.instance.collection("chats").document(chatId);
  }

  /// retorna la lista de usuarios del chat
  Future<List<User>> _getUsuarios() async {
    List<String> a = List<String>.from((await _chatDoc.get()).data['usuarios']);
    List<User> ans = [];
    for (int i = 0; i < a.length; i++) {
      String u = a[i];
      if (u == current.id) currentIdPos = i;
      ans.add(await _getUsuarioByID(u));
    }
    return ans;
  }

  /// Retorna los datos del usuario con el ID dado
  Future<User> _getUsuarioByID(String userID) async {
    DocumentSnapshot document =
        await Firestore.instance.collection("usuarios").document(userID).get();
    return  User.fromJson(document.documentID, document.data);
  }

  /// subir un mensage
  void post(Mensage msg) async {
    if (currentIdPos == null) usuarios();
    msg.userIdPos = currentIdPos;
    _chatDoc.collection('msgs').add(msg.toJson());
  }

  /// test to recive
  listener(OnMensage onMensage) {
    _chatDoc.collection('msgs').orderBy('date',descending: false).snapshots().listen((QuerySnapshot query) async{
      List<User> usuariosChat = await usuarios();
      var a = query.documentChanges;
      var ans = a.map((DocumentChange doc) => Mensage.fromJson(doc.document.data)).toList();
      for (Mensage b in ans) b.user = usuariosChat[b.userIdPos];

      for (Mensage b in ans) {
        print(b.toJson());
        onMensage(b);
      }
    });
  }
}

typedef OnMensage(Mensage m);
