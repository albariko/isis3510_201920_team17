import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';

///
/// Enum para manejar la respuesta de firebase
///
enum LoginStatus {
  Correct,
  NonEmail,
  IncorrectUserName,
  IncorrectUserPassword,
  NetworkError
}

///
/// Este es el provedor de de autehticacion de firebase
///
class AuthProvider {
  /// Este es el objeto del singleton
  static final AuthProvider _authProvider = AuthProvider._internal();

  /// Este es la conexion con firebase
  final FirebaseAuth _auth = FirebaseAuth.instance;

  /// Resultado de el loggin
  AuthResult result;

  /// Constructor publico
  factory AuthProvider() => _authProvider;

  /// Constructor privado
  AuthProvider._internal();

  /// Dar el id del usuario registrado
  String getUserId() => result.user.uid;

  /// Autentificacion
  Future<LoginStatus> authenticateUser(String email, String password) async {
    try {
      result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      return LoginStatus.Correct;
    } on PlatformException catch (e) {
      switch (e.message) {
        case 'There is no user record corresponding to this identifier. The user may have been deleted.':
          return LoginStatus.IncorrectUserName;
        case 'The password is invalid or the user does not have a password.':
          return LoginStatus.IncorrectUserPassword;
        case 'A network error (such as timeout, interrupted connection or unreachable host) has occurred.':
          return LoginStatus.NetworkError;
        case 'The email address is badly formatted.':
          return LoginStatus.NonEmail;
        default:
          print(e.message);
          throw e;
      }
    }
  }

  /// Registro
  handleSignUp(email, password) async {
    result = await _auth.createUserWithEmailAndPassword(
        email: email, password: password);
  }
}
